package com.itheima.takeout.presenter;

import android.os.Message;

import com.itheima.common.base.BaseView;
import com.itheima.takeout.model.protocol.BaseProtocol;
import com.itheima.takeout.model.protocol.CommonProtocol;

/**
 * Created by Administrator on 2017/4/21.
 *
 * @author hui
 */

public abstract class BasePresenter {
    public BaseView mBaseView;
    public CommonProtocol mProtocol;

    public BasePresenter(BaseView baseView){
        this.mBaseView = baseView;
        mProtocol =new CommonProtocol();
    }

    public BaseProtocol.HttpCallback mBaseCallback = new BaseProtocol.HttpCallback() {
        @Override
        public void onResponse(int reqType, Message msg) {

            mBaseView.onResponse(reqType,msg);
        }

        @Override
        public void onFailure(int reqType, String error) {
            mBaseView.onFailure(reqType,error);
        }
    };
}
