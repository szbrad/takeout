package com.itheima.takeout.presenter;

import android.os.Message;

import com.itheima.common.base.BaseView;
import com.itheima.takeout.model.bean.OrderBy;
import com.itheima.takeout.model.bean.ShopCategory;
import com.itheima.takeout.model.bean.ShopList;
import com.itheima.takeout.model.protocol.BaseProtocol;
import com.itheima.takeout.model.protocol.IHttpService;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2017/4/21.
 *
 * @author hui
 */

public class MainFragment01Presenter extends BasePresenter {


    private BaseProtocol.HttpCallback mCallback = new BaseProtocol.HttpCallback() {
        @Override
        public void onResponse(int reqType, Message msg) {
            //对数据进行解析,在将解析后的数据在进行回调显示
            if (reqType == IHttpService.GET_SHOP_LIST) {
                //将页数添加
                pageNo++;
                //如果回调请求的类型是请求商家列表
                //将获取到的ShopList数据转换为集合数据
                ArrayList pagerData = transformShopListData((ShopList) msg.obj);
                //将消息重新赋值
                msg.obj = pagerData;
                //将数据通过接口回调出去
                mBaseView.onResponse(reqType, msg);
                return;
            }
            //获取商家分类
            if (reqType == IHttpService.GET_SHOP_CATEGORY) {
                msg.obj = transformShopCategoryData((ShopCategory) msg.obj);
                mBaseView.onResponse(reqType, msg);
                return;
            }

            //获取商家排序条件
            if (reqType == IHttpService.GET_ORDERBY) {
                ArrayList<OrderBy.OrderByListBean> orderByListBeen  = new ArrayList<>();
                OrderBy orderBy = (OrderBy) msg.obj;
                if (orderBy!=null)
                   msg.obj =  orderBy.getOrderByList();
            }
            mBaseView.onResponse(reqType, msg);
        }

        @Override
        public void onFailure(int reqType, String error) {
            mBaseView.onFailure(reqType, error);
        }
    };

    /**
     * 对商家列表数据进行在处理商家列表
     *
     * @param category 商家列表数据
     * @return 处理后的商家列表集
     */
    private ArrayList<ShopCategory.CategoryListBean> transformShopCategoryData(ShopCategory category) {
        ArrayList<ShopCategory.CategoryListBean> categoryListBeen = new ArrayList<>();
        List<ShopCategory.CategoryListBean> categoryList = category.getCategoryList();
        for (int i = 0, size = categoryList.size(); i < size; i++) {
            //-1的表示没有二级菜单
            if (-1 == categoryList.get(i).getParentCategory()) {
                categoryListBeen.add(categoryList.get(i));
            }
        }
        //全部的分类条目数
        int allCategoryCount = 0;
        //将二级菜单添加到一级菜单中
        for (ShopCategory.CategoryListBean parent : categoryListBeen) {

            for (ShopCategory.CategoryListBean c : category.getCategoryList()) {
                if (c.getParentCategory() == parent.getId()) {
                    parent.childrenCategory.add(c);
                }
            }
            allCategoryCount += parent.getShopCount();
        }
        // (3) 添加"全部"选项
        ShopCategory.CategoryListBean allCategory = new ShopCategory.CategoryListBean();
        allCategory.setName("全部");
        allCategory.setId(0);
        allCategory.setShopCount(allCategoryCount);
        allCategory.setParentCategory(0);  // 没有父类别
        categoryListBeen.add(0, allCategory);
        return categoryListBeen;

    }


    private ArrayList transformShopListData(ShopList bean) {
        ArrayList<Object> datas = new ArrayList<Object>();
        //附近商家
        List<ShopList.ShopListBean> shopList = bean.getShopList();
        //将附近商家的10 条数据添加到集合中
        datas.addAll(shopList);
        if (bean.getShopList() != null && bean.getRecommendList() != null && bean.getRecommendList().size() > 0) {
            //广告
            List<ShopList.RecommendListBean> recommendList = bean.getRecommendList();
            //将第一条广告添加到集合中
            datas.add(recommendList.get(0));
        }

        return datas;

    }

    public MainFragment01Presenter(BaseView baseView) {
        super(baseView);
    }

    /**
     * 获取头部促销展示
     */
    public void getHomeBean() {
        mProtocol.getHomeBean(mBaseCallback);
    }

    /**
     * 获取商家列表
     */
    public void getShopList() {
        mProtocol.getShopListBean(mCallback, pageNo, pageCount);
    }

    /**
     * 获取商家分类
     */
    public void getShopCategory() {
        mProtocol.getShopCateGory(mCallback);
    }

    /**
     * 获取商家排序条件
     */
    public void getOrderBy() {
        mProtocol.getOrderBy(mCallback);
    }


    //加载的是第几页
    private int pageNo = 1;
    //需要加载的条目数
    private int pageCount = 10;

    /**
     * 加载商品列表的数据
     *
     * @param firstPager 是否是第一页数据
     */
    public void getShopListData(boolean firstPager) {
        if (firstPager) pageNo = 1;
        mProtocol.getShopListBean(mCallback, pageNo, pageCount);
    }

    /**
     * 获取分页数据
     */
    public void getShopListData(boolean firstPager, int categorycategoryId, int orderorderBy) {
        //,int categoryId,int orderBy
        if (firstPager) pageNo = 1;
        mProtocol.getShopListBean(mCallback, pageNo, pageCount, categorycategoryId, orderorderBy);
    }


}
