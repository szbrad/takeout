package com.itheima.takeout.presenter;

import android.os.Message;

import com.itheima.common.base.BaseView;
import com.itheima.takeout.model.bean.ShopDetail;
import com.itheima.takeout.model.bean.local.CartInfo;
import com.itheima.takeout.model.bean.local.GoodsData;
import com.itheima.takeout.model.dao.MyCartInfoDao;
import com.itheima.takeout.model.protocol.BaseProtocol;
import com.itheima.takeout.model.protocol.IHttpService;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2017/4/25.
 *
 * @author hui
 */

public class ShopDetailFragment1Presenter extends BasePresenter {

    private GoodsData mGoodsData;
    private List<ShopDetail.CategoryBean.GoodsBean> mAllCartData;

    public ShopDetailFragment1Presenter(BaseView baseView) {
        super(baseView);
    }


    public void getShopDetail(int shopId) {
        mProtocol.getShopDetail(mCallback, shopId);
    }

    private BaseProtocol.HttpCallback mCallback = new BaseProtocol.HttpCallback() {
        @Override
        public void onResponse(int reqType, Message msg) {

            if (reqType == IHttpService.GET_SHOP_DETAIL) {

                msg.obj = transformShopDetailData((ShopDetail) msg.obj);
            }
            mBaseView.onResponse(reqType, msg);

        }

        @Override
        public void onFailure(int reqType, String error) {
            mBaseView.onFailure(reqType, error);
        }
    };

    /**
     * 根据商品类别的id获取右侧列表要滚动到的位置
     *
     * @param categoryId
     * @return
     */
    public int getListViewPos(int categoryId) {
        for (int i = 0, size = mGoodsData.getAllGoods().size(); i < size; i++) {
            int categoryId1 = mGoodsData.getAllGoods().get(i).getCategoryId();
            if (categoryId == categoryId1) {
                return i;
            }
        }
        return 0;
    }

    /**
     * 获取购物车购买数量和购买的金额
     *
     * @return 购物车实体类
     */
    public CartInfo getCartInfo() {
        CartInfo cartInfo = new CartInfo();
        List<ShopDetail.CategoryBean.GoodsBean> allGoods = mGoodsData.getAllGoods();
        if (allGoods == null || allGoods.size() < 1) {
            return cartInfo;
        }

        for (int i = 0; i < allGoods.size(); i++) {
            ShopDetail.CategoryBean.GoodsBean goodsBean = allGoods.get(i);
            if (goodsBean.mBuyCount > 0) {
                cartInfo.setBuyCount(cartInfo.getBuyCount() + goodsBean.mBuyCount);
                cartInfo.setAmount(cartInfo.getAmount() + goodsBean.mBuyCount * goodsBean.getNewPrice());

            }
        }

        return cartInfo;
    }

    /**
     * 清空某个商家购物车数据
     */
    public void clearCartDatas() {
        if (mGoodsData != null && mGoodsData.getAllGoods() != null) {
            for (ShopDetail.CategoryBean.GoodsBean goods : mGoodsData.getAllGoods()) {
                // 购物车的数量大于0时，要置为0，清空购买数量
                if (goods.mBuyCount > 0) {
                    goods.mBuyCount = 0;
                }
            }
        }
    }

    /**
     * 转换数据
     *
     * @param detail 商家详情
     * @return 商家商品的详情
     */
    private GoodsData transformShopDetailData(ShopDetail detail) {
        List<ShopDetail.CategoryBean> category = detail.getCategory();
        List<ShopDetail.CategoryBean.GoodsBean> allGoods = new ArrayList<>();
        List<com.itheima.takeout.db.greendao.CartInfo> allCacheData = cartInfoDao.queryAll();
        //遍历所有商品类别
        // 遍历所有的商品类别
        for (ShopDetail.CategoryBean c : detail.getCategory()) {
            // 遍历该类别下所有的商品
            for (ShopDetail.CategoryBean.GoodsBean goods : c.getGoods()) {
                goods.categoryName = c.getName(); // 绑定商品类别名称
                allGoods.add(goods);

                // 刷新该商家添加到购物车的数量
                for (com.itheima.takeout.db.greendao.CartInfo cacheBean : allCacheData) {
                    if (cacheBean.getGoodsId() == goods.getId()) {
                        // 更新当前商品添加到购物车的数量
                        goods.mBuyCount = cacheBean.getCount();
                        break;
                    }
                }
            }
        }
        mGoodsData = new GoodsData();
        mGoodsData.setAllGoods(allGoods);
        mGoodsData.setCategoryBeen(category);
        return mGoodsData;
    }

    public List<ShopDetail.CategoryBean.GoodsBean> getAllCartData() {
        List<ShopDetail.CategoryBean.GoodsBean> shoppingCartData = new ArrayList<>();

        List<ShopDetail.CategoryBean.GoodsBean> allGoods = mGoodsData.getAllGoods();
        if (allGoods == null || allGoods.size() < 1)
            return shoppingCartData;
        for (int i = 0; i < allGoods.size(); i++) {
            ShopDetail.CategoryBean.GoodsBean goodsBean = allGoods.get(i);
            if (goodsBean.mBuyCount > 0) {
                shoppingCartData.add(goodsBean);
            }
        }

        return shoppingCartData;
    }

    MyCartInfoDao cartInfoDao = new MyCartInfoDao();

    public MyCartInfoDao getCartInfoDao() {
        return cartInfoDao;
    }
}
