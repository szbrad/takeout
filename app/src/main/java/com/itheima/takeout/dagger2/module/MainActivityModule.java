package com.itheima.takeout.dagger2.module;

import com.itheima.common.base.BaseView;
import com.itheima.takeout.presenter.MainFragment01Presenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Administrator on 2017/4/21.
 *
 * @author hui
 */
@Module
public class MainActivityModule {

    private BaseView mBaseView;


    public MainActivityModule(BaseView baseView) {
        this.mBaseView = baseView;
    }
    @Provides
    public BaseView providerBaseView() {
        return mBaseView;
    }
    @Provides
    public MainFragment01Presenter providerMainFragment01Presenter() {
        return new MainFragment01Presenter(mBaseView);
    }


}
