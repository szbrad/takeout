package com.itheima.takeout.dagger2.component;

import com.itheima.takeout.dagger2.module.MainActivityModule;
import com.itheima.takeout.ui.fragment.MainFragment01;

import dagger.Component;

/**
 * Created by Administrator on 2017/4/21.
 *
 * @author hui
 */
@Component(modules = MainActivityModule.class)
public interface MainActivityComponent {
    public void inject(MainFragment01 fragment1);

}
