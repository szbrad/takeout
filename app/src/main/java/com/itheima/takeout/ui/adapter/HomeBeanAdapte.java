package com.itheima.takeout.ui.adapter;

import android.content.Context;
import android.view.ViewGroup;

import com.itheima.common.ui.BaseAdapterRV;
import com.itheima.common.ui.BaseHolderRV;
import com.itheima.takeout.model.bean.HomeBean;
import com.itheima.takeout.model.bean.ShopList;
import com.itheima.takeout.ui.holder.HomeBeanHolder;
import com.itheima.takeout.ui.holder.RecommendHolder;
import com.itheima.takeout.ui.holder.ShopBeanHolder;

import java.util.List;

/**
 * Created by Administrator on 2017/4/21.
 *
 * @author hui
 */

public class HomeBeanAdapte extends BaseAdapterRV<Object> {
    public static final int ITEM_TYPE_HEADER = 0;   // 列表项：头部
    public static final int ITEM_TYPE_SHOP = 1;     // 列表项：商家
    public static final int ITEM_TYPE_AD = 2;       // 列表项：广告

    public HomeBeanAdapte(Context context, List listData) {
        super(context, listData);
    }

    @Override
    public BaseHolderRV createViewHolder(Context context, ViewGroup parent, int viewType) {
        switch(viewType){
            case ITEM_TYPE_HEADER:
                return new HomeBeanHolder(context,parent,this,viewType);
            case ITEM_TYPE_SHOP:
                return new ShopBeanHolder(context,parent,this,viewType);
            case ITEM_TYPE_AD:
                return new RecommendHolder(context,parent,this,viewType);
        }
        throw new IllegalStateException("无法识别的列表项类型");

    }

    @Override
    public int getItemViewType(int position) {
        Object item = getItem(position);
        if (item instanceof HomeBean) {
            return ITEM_TYPE_HEADER;
        }
        if (item instanceof ShopList.ShopListBean) {
            return ITEM_TYPE_SHOP;
        }
        if (item instanceof ShopList.RecommendListBean) {
            return ITEM_TYPE_AD;
        }
        throw new IllegalStateException("无法识别的列表项类型" + item);
    }
}
