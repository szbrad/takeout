package com.itheima.takeout.ui.adapter;

import android.content.Context;
import android.view.ViewGroup;

import com.itheima.common.ui.BaseAdapterLV;
import com.itheima.common.ui.BaseHolderLV;
import com.itheima.takeout.model.bean.ShopCategory;
import com.itheima.takeout.ui.holder.ShopCategoryParentHolder;

import java.util.List;

/**
 * Created by Administrator on 2017/4/24.
 *
 * @author hui
 */

public class ShopCategoryParentAdapter extends BaseAdapterLV<ShopCategory.CategoryListBean> {


    //当前选中的View
    public  ShopCategory.CategoryListBean mSelectCategory;

    public ShopCategoryParentAdapter(Context context, List<ShopCategory.CategoryListBean> listData) {
        super(context, listData);
    }

    @Override
    public BaseHolderLV<ShopCategory.CategoryListBean> createViewHolder(Context context, ViewGroup parent, ShopCategory.CategoryListBean bean, int position) {
        return new ShopCategoryParentHolder(context,parent,this,position,bean);
    }
}
