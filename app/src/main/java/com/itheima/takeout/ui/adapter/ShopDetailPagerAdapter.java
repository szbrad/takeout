package com.itheima.takeout.ui.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.List;

/**
 * Created by Administrator on 2017/4/25.
 *
 * @author hui
 */

public class ShopDetailPagerAdapter extends FragmentPagerAdapter {
    private List<Fragment> mFragments;
    private String[] mTitile;
    public ShopDetailPagerAdapter(FragmentManager fm,List<Fragment> fragments) {
        super(fm);
        this.mFragments = fragments;
    }

    @Override
    public Fragment getItem(int position) {
        return mFragments.get(position);
    }

    @Override
    public int getCount() {
        return mFragments!=null?mFragments.size():0;
    }
    public void setData(List<Fragment> fragments,String[] title){
        this.mFragments = fragments;
        this.mTitile = title;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mTitile[position];
    }
}
