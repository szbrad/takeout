package com.itheima.takeout.ui.holder;

import android.content.Context;
import android.content.res.Resources;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.itheima.common.base.Global;
import com.itheima.common.ui.BaseAdapterLV;
import com.itheima.common.ui.BaseHolderLV;
import com.itheima.takeout.R;
import com.itheima.takeout.model.bean.OrderBy;
import com.itheima.takeout.ui.adapter.OrderByAdapter;

/**
 * Created by Administrator on 2017/4/25.
 *
 * @author hui
 */

public class OrderByHolder extends BaseHolderLV<OrderBy.OrderByListBean>{

    private TextView mTvTitle;

    public OrderByHolder(Context context, ViewGroup parent, BaseAdapterLV<OrderBy.OrderByListBean> adapter, int position, OrderBy.OrderByListBean bean) {
        super(context, parent, adapter, position, bean);
    }

    @Override
    public View onCreateView(Context context, ViewGroup parent) {
        View orderByView = Global.inflate(R.layout.item_order_by, parent);
        mTvTitle = (TextView) orderByView.findViewById(R.id.tv_title);
        return orderByView;
    }

    @Override
    protected void onRefreshView(OrderBy.OrderByListBean bean, int position) {
        mTvTitle.setText(bean.getName());
        Resources resources = context.getResources();
        OrderBy.OrderByListBean selectOrderBy = ((OrderByAdapter) adapter).mSelectOrderBy;
        if (selectOrderBy!=null && bean.getId() == selectOrderBy.getId()){
            mTvTitle.setTextColor(resources.getColor(R.color.shop_category_item_selected));
        }else{
            mTvTitle.setTextColor(resources.getColor(R.color.list_item_text_01));
        }

    }
}
