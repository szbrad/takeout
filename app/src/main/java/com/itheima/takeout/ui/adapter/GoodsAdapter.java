package com.itheima.takeout.ui.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.itheima.common.base.Global;
import com.itheima.common.ui.BaseAdapterLV;
import com.itheima.common.ui.BaseHolderLV;
import com.itheima.takeout.R;
import com.itheima.takeout.model.bean.ShopDetail;
import com.itheima.takeout.ui.holder.GoodsHolder;

import java.util.List;

import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;

/**
 * Created by Administrator on 2017/4/26.
 *
 * @author hui
 */

public class GoodsAdapter extends BaseAdapterLV<ShopDetail.CategoryBean.GoodsBean> implements StickyListHeadersAdapter {



    public GoodsAdapter(Context context, List<ShopDetail.CategoryBean.GoodsBean> listData) {
        super(context, listData);
    }

    @Override
    public BaseHolderLV<ShopDetail.CategoryBean.GoodsBean> createViewHolder(Context context, ViewGroup parent, ShopDetail.CategoryBean.GoodsBean bean, int position) {
        return new GoodsHolder(context,parent,this,position,bean);
    }

    @Override
    public View getHeaderView(int position, View convertView, ViewGroup parent) {
        if (convertView==null){
            convertView = Global.inflate(R.layout.item_goods_category_header, parent);
        }
        TextView tvGoodsCategory  = (TextView) convertView.findViewById(R.id.tv_goods_category);
        tvGoodsCategory.setText(getItem(position).categoryName);
        return convertView;
    }

    @Override
    public long getHeaderId(int position) {
        return getItem(position).getCategoryId();
    }
}
