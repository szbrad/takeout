package com.itheima.takeout.ui.activity;

import android.os.Message;
import android.view.View;
import android.widget.Button;

import com.itheima.common.base.BaseActivity;
import com.itheima.takeout.R;

/**
 * Created by Administrator on 2017/4/27.
 *
 * @author hui
 */

public class AddressManageActivity extends BaseActivity {

    private Button mBtnSaveAddress;

    @Override
    public int getLayoutRes() {
        return R.layout.activity_add_recipients_address;
    }

    @Override
    public void initView() {
        mBtnSaveAddress = findView(R.id.btn_save_address);

    }

    @Override
    public void initListener() {

    }

    @Override
    public void initData() {

    }

    @Override
    public void onClick(View v, int id) {
        if (mBtnSaveAddress == v){
            showToast("保存成功");

        }

    }

    @Override
    public void onResponse(int reqType, Message msg) {

    }
}
