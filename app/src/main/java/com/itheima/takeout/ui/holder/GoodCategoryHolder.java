package com.itheima.takeout.ui.holder;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.itheima.common.base.Global;
import com.itheima.common.ui.BaseAdapterRV;
import com.itheima.common.ui.BaseHolderRV;
import com.itheima.takeout.R;
import com.itheima.takeout.model.bean.ShopDetail;
import com.itheima.takeout.ui.activity.ShopDetailActivity;
import com.itheima.takeout.ui.adapter.GoodCategoryAdapter;
import com.itheima.takeout.ui.fragment.ShopDetailFragment1;

/**
 * Created by Administrator on 2017/4/26.
 *
 * @author hui
 */

public class GoodCategoryHolder extends BaseHolderRV<ShopDetail.CategoryBean> {

    private TextView mTvGoodsCategory;

    public GoodCategoryHolder(Context context, ViewGroup parent, BaseAdapterRV<ShopDetail.CategoryBean> adapter, int itemType) {
        super(context, parent, adapter, itemType, R.layout.item_goods_category);
    }

    @Override
    public void onFindViews(View itemView) {
//        tv_cart_goods_amount
//        tv_goods_category
        TextView tvCartGoodsAmount = (TextView) itemView.findViewById(R.id.tv_cart_goods_amount);
        mTvGoodsCategory = (TextView) itemView.findViewById(R.id.tv_goods_category);

    }

    @Override
    protected void onRefreshView(ShopDetail.CategoryBean bean, int position) {
        mTvGoodsCategory.setText(bean.getName());
        ShopDetail.CategoryBean selectedBean = ((GoodCategoryAdapter) adapter).mSelectedBean;
        if (selectedBean!=null&&selectedBean.getId() == bean.getId()){
            // 高亮显示
            mTvGoodsCategory.setTextColor(Global.getColor(R.color.title_bar_bg));
            super.itemView.setBackgroundColor(Color.WHITE);  // 列表项的背景颜色
        }else{
            mTvGoodsCategory.setTextColor(Global.getColor(R.color.item_text_02));
            super.itemView.setBackgroundColor(Color.TRANSPARENT);
        }
    }

    @Override
    protected void onItemClick(View itemView, int position, ShopDetail.CategoryBean bean) {
        ((GoodCategoryAdapter) adapter).mSelectedBean = bean;
        adapter.notifyDataSetChanged();
        ShopDetailFragment1 fragment1 = ((ShopDetailActivity) super.context).getFragment1();
        fragment1.getStickyListHeadersListView().setSelection(fragment1.getPresenter().getListViewPos(bean.getId()));


    }
}
