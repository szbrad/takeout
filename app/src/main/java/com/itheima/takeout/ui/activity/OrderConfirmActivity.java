package com.itheima.takeout.ui.activity;

import android.os.Message;
import android.view.View;
import android.widget.LinearLayout;

import com.itheima.common.base.BaseActivity;
import com.itheima.takeout.R;

/**
 * Created by Administrator on 2017/4/27.
 *
 * @author hui
 */

public class OrderConfirmActivity extends BaseActivity {

    private LinearLayout mLlRecipients;

    @Override
    public int getLayoutRes() {
        return R.layout.activity_confirm_order;
    }

    @Override
    public void initView() {
        mLlRecipients = findView(R.id.ll_recipients);

    }

    @Override
    public void initListener() {
        mLlRecipients.setOnClickListener(this);

    }

    @Override
    public void initData() {

    }

    @Override
    public void onClick(View v, int id) {
        switch (id){
            case R.id.ll_recipients:
                //TODO 跳转到选择地址界面
//                SelectAddressActivity?

                break;

        }


    }

    @Override
    public void onResponse(int reqType, Message msg) {

    }
}
