package com.itheima.takeout.ui.fragment;

import android.graphics.Color;
import android.os.Message;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.itheima.common.base.BaseFragment;
import com.itheima.common.base.Global;
import com.itheima.common.util.LogUtil;
import com.itheima.takeout.R;
import com.itheima.takeout.dagger2.component.DaggerMainActivityComponent;
import com.itheima.takeout.dagger2.module.MainActivityModule;
import com.itheima.takeout.model.bean.HomeBean;
import com.itheima.takeout.model.bean.OrderBy;
import com.itheima.takeout.model.bean.ShopCategory;
import com.itheima.takeout.model.bean.ShopList;
import com.itheima.takeout.model.protocol.BaseProtocol;
import com.itheima.takeout.model.protocol.IHttpService;
import com.itheima.takeout.presenter.MainFragment01Presenter;
import com.itheima.takeout.ui.adapter.HomeBeanAdapte;
import com.itheima.takeout.ui.adapter.OrderByAdapter;
import com.itheima.takeout.ui.adapter.ShopCategoryParentAdapter;
import com.itheima.takeout.ui.view.BaiduHeader;
import com.liaoinstan.springview.container.MeituanFooter;
import com.liaoinstan.springview.widget.SpringView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by Administrator on 2017/4/21.
 *
 * @author hui
 */

public class MainFragment01 extends BaseFragment implements BaseProtocol.HttpCallback {
    @Inject
    MainFragment01Presenter mPresenter;
    private HomeBean mHomeBean;
    private List<ShopList.RecommendListBean> mRecommendList;
    private List<ShopList.ShopListBean> mShopListBeen;
    private HomeBeanAdapte mAdapter;

    private SpringView mSpringView;
    private TextView mTvLocation;
    private TextView mTvSearch01;
    private LinearLayout mLlTitleBar2;
    private LinearLayout mLlTitleBar2Left;
    private CheckBox mCbCategory;
    private CheckBox mCbOrderBy;
    private RecyclerView mRecyclerView;
    private int mLlTitleBar2LeftWidth;
    private int mLlTitleBar2RightWidth;
    private TextView mLlTitleBar2Right;
    private LinearLayout mLlTitleBar1;
    private LinearLayout mLlTopLayout;
    private List<Object> mData;
    private LinearLayout mLlPopRoot01;
    private LinearLayout mLlPopRoot02;
    private LinearLayout mLlPopContent01Category;
    private LinearLayout mLlPopContent02OrderBy;
    private ListView mLvCategory01;
    private ListView mLvCategory02;
    private ListView mLvOrderBy;
    private ShopCategoryParentAdapter mShopCategoryParentAdapter01;
    private ShopCategoryParentAdapter mShopCategoryParentAdapter02;
    private OrderByAdapter mOrderByAdapter;


    @Override
    public int getLayoutRes() {
        return R.layout.fragment_01;
    }

    @Override
    public void initView() {
        initRecyclerView();
        initSpringView();
        initTitleBar();
//        ll_pop_root_02
        initPopRoot();
    }

    private void initPopRoot() {
//        ll_pop_root_02
        mLlPopRoot01 = findView(R.id.ll_pop_root_01);
        mLlPopRoot02 = findView(R.id.ll_pop_root_02);

        mLlPopContent01Category = findView(R.id.ll_pop_content_01_category);
        mLlPopContent02OrderBy = findView(R.id.ll_pop_content_02_order_by);

        // 列表
        mLvCategory01 = findView(R.id.lv_category_01);
        mLvCategory02 = findView(R.id.lv_category_02);
        mLvOrderBy = findView(R.id.lv_order_by);

        // 去掉分割线
        mLvCategory01.setDividerHeight(0);
        mLvCategory02.setDividerHeight(0);
        mLvOrderBy.setDividerHeight(0);

        //适配器
        mShopCategoryParentAdapter01 = new ShopCategoryParentAdapter(mActivity, null);
        mLvCategory01.setAdapter(mShopCategoryParentAdapter01);
        mShopCategoryParentAdapter02 = new ShopCategoryParentAdapter(mActivity, null);
        mLvCategory02.setAdapter(mShopCategoryParentAdapter02);

        mOrderByAdapter = new OrderByAdapter(mActivity, null);
        mLvOrderBy.setAdapter(mOrderByAdapter);

    }


    /**
     * 初始化标题栏
     */
    private void initTitleBar() {

        // 标题栏1
        mLlTitleBar1 = (LinearLayout) findView(R.id.ll_title_bar1);
        mTvLocation = findView(R.id.tv_location);
        mTvSearch01 = findView(R.id.tv_search_01);

        // 标题栏2
        mLlTitleBar2 = findView(R.id.ll_title_bar2);
        mLlTitleBar2Left = findView(R.id.ll_title_bar2_left);
        mLlTitleBar2Right = findView(R.id.ll_title_bar2_right);
        //顶部弹窗
        mLlTopLayout = findView(R.id.ll_top_layout);
        mCbCategory = findView(R.id.cb_category);
        mCbOrderBy = findView(R.id.cb_orderby);

        mCbCategory.setOnClickListener(this);
        mCbOrderBy.setOnClickListener(this);
        //隐藏标题栏2左右两部分
        //标题栏左边的宽度
        mLlTitleBar2LeftWidth = Global.dp2px(100);
        //标题栏右边的宽度
        mLlTitleBar2RightWidth = (int) Global.mScreenWidth - mLlTitleBar2LeftWidth;
        //左边的标题移动 - 的左边宽度
        mLlTitleBar2Left.setTranslationX(-mLlTitleBar2LeftWidth);
        //向右边移动多少距离
        mLlTitleBar2Right.setTranslationX(mLlTitleBar2RightWidth);

        //状态栏完全透明
        Global.setNoStatusBarFullMode(mActivity);
        Global.setStatusBarPadding(mLlTitleBar1);
        Global.setStatusBarPadding(mLlTopLayout);


    }


    /**
     * 初始化RecyuclerView
     */
    private void initRecyclerView() {
        mRecyclerView = findView(R.id.recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mActivity));
        mAdapter = new HomeBeanAdapte(mActivity, null);
        mRecyclerView.setAdapter(mAdapter);

    }

    /**
     * 初始化SpringView 及 SpringView的下拉刷新和上拉加载事件
     */
    private void initSpringView() {
        mSpringView = findView(R.id.spring_view);
        //设置SpringView的头布局
        mSpringView.setHeader(new BaiduHeader(super.getContext()));
        //设置SpringView的尾布局
        mSpringView.setFooter(new MeituanFooter(super.getContext()));
        //设置SprongView的展示类型
        mSpringView.setType(SpringView.Type.FOLLOW);
        //设置SpringView的监听器
        mSpringView.setListener(new SpringView.OnFreshListener() {
            @Override
            public void onRefresh() {
                //下拉的时候
                mPresenter.getHomeBean();

            }

            @Override
            public void onLoadmore() {
                //上拉加载更多
                mPresenter.getShopList();
            }
        });
    }


    /**
     * 设置RecyclerView的滑动监听
     */
    @Override
    public void initListener() {
        mRecyclerView.setOnScrollListener(mOnScrollListener);
        mLvCategory01.setOnItemClickListener(mItemClickListener);
        mLvCategory02.setOnItemClickListener(mItemClickListener);
        mLvOrderBy.setOnItemClickListener(mItemClickListener);

    }

    //标题切换的临界点  --->小于180dp
    int mRange = Global.dp2px(110);
    //获取屏幕垂直方向滑动的距离
    private int mDistanceY = 0;
    //标题栏2是否显示
    private boolean isTitleBar2Show = false;

    /**
     * 滑动列表的监听
     */
    private RecyclerView.OnScrollListener mOnScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            mDistanceY += dy;
            //设置标题栏1 的透明度
            if (mDistanceY < 0) {
                //往上滑动的时候
                mTvLocation.setAlpha(1);
                mTvSearch01.setAlpha(1);
            } else {
                //往下滑动的时候
                float percent = Math.min(mDistanceY, mRange) / ((float) mRange);
                mTvSearch01.setAlpha(1 - percent);
                mTvLocation.setAlpha(1 - percent);
            }
            //显示或隐藏标题栏2
            //隐藏标题栏2
            if (!isTitleBar2Show && mDistanceY > mRange) {
                //隐藏----->显示
                isTitleBar2Show = true;
                showTitleBar2();
                //设置状态栏的颜色为黑色
                Global.setStatusBarColor(getActivity(), Color.BLACK);
            } else if (isTitleBar2Show && mDistanceY < mRange) {
                //显示---->隐藏
                isTitleBar2Show = false;
                hideTitleBar2();
                Global.getMainHandler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //设置状态栏的颜色为透明
                        Global.setNoStatusBarFullMode(getActivity());
                    }
                }, 200);

            }


            super.onScrolled(recyclerView, dx, dy);
        }
    };

    /**
     * 隐藏标题栏2
     */
    private void hideTitleBar2() {
        mLlTitleBar2Left.animate().translationX(-mLlTitleBar2LeftWidth);
        mLlTitleBar2Right.animate().translationX(mLlTitleBar2RightWidth);
        Global.getMainHandler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mLlTitleBar2.setVisibility(View.GONE);
            }
        }, 200);
    }

    /**
     * 显示标题栏2
     */
    private void showTitleBar2() {
        mLlTitleBar2.setVisibility(View.VISIBLE);
        //移动到原始位置   translationX 正值表示往右移动,负值表示往左移动   0表示移动到原始位置
        mLlTitleBar2Left.animate().translationX(0).setDuration(300);
        mLlTitleBar2Right.animate().translationX(0).setDuration(300);
    }

    @Override
    public void initData() {
        //注入Presenter对象
        DaggerMainActivityComponent.builder().mainActivityModule(new MainActivityModule(this)).build().inject(this);
        mPresenter.getHomeBean();
        mPresenter.getShopCategory();
        mPresenter.getOrderBy();
    }

    AdapterView.OnItemClickListener mItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            //如果适配器的父布局是左边的ListView
            if (parent == mLvCategory01) {

                ShopCategory.CategoryListBean itemAtPosition = (ShopCategory.CategoryListBean) parent.getItemAtPosition(position);
                ArrayList<ShopCategory.CategoryListBean> childrenCategory = itemAtPosition.childrenCategory;
                if (childrenCategory != null && childrenCategory.size() > 0) {
                    mShopCategoryParentAdapter02.setDatas(childrenCategory);
                } else {
                    mShopCategoryParentAdapter02.setDatas(childrenCategory);
                }
                LogUtil.i("当前选中的控件为 + " + itemAtPosition.getName());
                mShopCategoryParentAdapter01.mSelectCategory = itemAtPosition;
                // （3）根据选择的类别搜索商家
                boolean hasChildrenCategory = itemAtPosition.childrenCategory != null
                        && itemAtPosition.childrenCategory.size() > 0;
                mSelectCategory = itemAtPosition;
                if (hasChildrenCategory) {

                } else {

                    mPresenter.getShopListData(true, getCategoryId(), getOrderByTag());
                    // 隐藏弹出窗口
                    showOrHideCategoryLayout();
                }
                //刷新列表
                mShopCategoryParentAdapter01.notifyDataSetChanged();
            }
            //如果适配器的父布局是商家右边的ListView
            if (parent == mLvCategory02) {
                ShopCategory.CategoryListBean itemAtPosition = (ShopCategory.CategoryListBean) parent.getItemAtPosition(position);
                ArrayList<ShopCategory.CategoryListBean> childrenCategory = itemAtPosition.childrenCategory;
                LogUtil.i("当前选中的控件为 + " + itemAtPosition.getName());
                mShopCategoryParentAdapter02.mSelectCategory = itemAtPosition;
                mSelectCategory = itemAtPosition;

                boolean hasChildrenCategory = itemAtPosition.childrenCategory != null
                        && itemAtPosition.childrenCategory.size() > 0;
                if (hasChildrenCategory) {
                } else {
                    mPresenter.getShopListData(true, getCategoryId(), getOrderByTag());
                    // 隐藏弹出窗口
                    showOrHideCategoryLayout();
                }
                //刷新列表
                mShopCategoryParentAdapter02.notifyDataSetChanged();
            }

            //如果适配器的父布局是排序的ListView
            if (parent == mLvOrderBy) {
                //获取当前点击的条目对应的javaBean对象
                OrderBy.OrderByListBean itemAtPosition = (OrderBy.OrderByListBean) parent.getItemAtPosition(position);
                mOrderByAdapter.mSelectOrderBy = itemAtPosition;
                mSelectOrderBy = itemAtPosition;
                mPresenter.getShopListData(true, getCategoryId(), getOrderByTag());
                // 隐藏弹出窗口
                showOrHideOrderByLayout();


                //刷新列表
                mOrderByAdapter.notifyDataSetChanged();


            }


        }
    };


    private ShopCategory.CategoryListBean mSelectCategory;
    private OrderBy.OrderByListBean mSelectOrderBy;

    /**
     * 获取商家分类的id
     */
    private int getCategoryId() {
        return (mSelectCategory == null) ? 0 : mSelectCategory.getId();
    }

    /**
     * 获取商家排序条件的标签
     */
    public int getOrderByTag() {
        return (mSelectOrderBy == null) ? 0 : mSelectOrderBy.getTag();
    }


    @Override
    public void onClick(View v, int id) {

        switch (id) {
            case R.id.cb_category: //商家分类
                showOrHideCategoryLayout();
                break;
            case R.id.cb_orderby: //排序条
                showOrHideOrderByLayout();
                break;
        }
    }


    /**
     * 显示或隐藏类别弹出窗布局
     */
    private void showOrHideCategoryLayout() {
        if (mLlPopRoot01.getVisibility() == View.GONE) {
            // 隐藏 -> 显示
            mLlPopRoot02.setVisibility(View.GONE);
            mLlPopRoot01.setVisibility(View.VISIBLE);
        } else {
            // 显示 -> 隐藏
            mLlPopRoot01.setVisibility(View.GONE);
            mCbCategory.setChecked(false);
        }
    }

    /**
     * 显示或隐藏排序条件弹出窗布局
     */
    private void showOrHideOrderByLayout() {
        if (mLlPopRoot02.getVisibility() == View.GONE) {
            // 隐藏 -> 显示
            mLlPopRoot02.setVisibility(View.VISIBLE);
            mLlPopRoot01.setVisibility(View.GONE);
        } else {
            // 显示 -> 隐藏
            mLlPopRoot02.setVisibility(View.GONE);
        }
    }


    @Override
    public void onResponse(int reqType, Message msg) {
        //当请求的类型是头部促销展示的时候
        if (reqType == IHttpService.GET_HOME) {
            // 当时下拉的时候,直接重新设置数据.
            mData = new ArrayList<Object>();
            mHomeBean = (HomeBean) msg.obj;
            mData.add(mHomeBean);
            //请求第一页的附近商家的数据
            mPresenter.getShopListData(true);
            return;
        }
        //当请求的类型是商家列表的时候
        if (reqType == IHttpService.GET_SHOP_LIST) {
            //数据请求成功后 隐藏SpringView控件
            mSpringView.onFinishFreshAndLoad();
            ArrayList<Object> pagerData = (ArrayList<Object>) msg.obj;
            //判断是下拉刷新还是加载更多
            if (msg.what == 1) { // 下拉刷新  重置列表数据
                pagerData.add(0, mData.get(0));
                mData = pagerData;
            } else { // 上拉加载更多
                mData.addAll(pagerData);
            }
            //刷新列表数据
            mAdapter.setDatas(mData);
            return;
        }

        //当请求的类型是商家类别的时候
        if (reqType == IHttpService.GET_SHOP_CATEGORY) {
            ArrayList<ShopCategory.CategoryListBean> shopCategoryList = (ArrayList) msg.obj;
            LogUtil.i("shopCategoryList" + shopCategoryList.size());
            mShopCategoryParentAdapter01.setDatas(shopCategoryList);
        }
        //当请求的类型是商家排序条件的时候
        if (reqType == IHttpService.GET_ORDERBY) {
            ArrayList<OrderBy.OrderByListBean> orderByListBeen = (ArrayList<OrderBy.OrderByListBean>) msg.obj;
            LogUtil.i("获取商家类别的个数为: " + orderByListBeen.size());
            if (orderByListBeen != null)
                mOrderByAdapter.setDatas(orderByListBeen);
        }

    }


}