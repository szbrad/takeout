package com.itheima.takeout.ui.holder;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.itheima.common.base.Global;
import com.itheima.common.ui.BaseAdapterRV;
import com.itheima.common.ui.BaseHolderRV;
import com.itheima.common.util.Utils;
import com.itheima.takeout.R;
import com.itheima.takeout.model.bean.ShopList;
import com.squareup.picasso.Picasso;

/**
 * Created by Administrator on 2017/4/21.
 *
 * @author hui
 */

public class RecommendHolder extends BaseHolderRV<ShopList.RecommendListBean> {

    private ImageView mIvAdImage;
    private TextView mTvAdTitle;

    public RecommendHolder(Context context, ViewGroup parent, BaseAdapterRV adapter, int itemType) {
        super(context, parent, adapter, itemType, R.layout.item_home_ad);
    }

    @Override
    public void onFindViews(View itemView) {
        mIvAdImage = (ImageView) itemView.findViewById(R.id.iv_ad_image);
        mTvAdTitle = (TextView) itemView.findViewById(R.id.tv_ad_title);
    }

    @Override
    protected void onRefreshView(ShopList.RecommendListBean bean, int position) {
        String imgUrl = Utils.replaceIp(bean.getImage());
        Picasso.with(super.context).load(imgUrl).into(mIvAdImage);
        mTvAdTitle.setText(bean.getTitle());


    }

    @Override
    protected void onItemClick(View itemView, int position, ShopList.RecommendListBean bean) {
        Global.showToast("HelloWorld" + bean.getTitle());
    }
}
