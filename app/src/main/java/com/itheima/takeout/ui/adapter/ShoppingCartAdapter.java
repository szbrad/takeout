package com.itheima.takeout.ui.adapter;

import android.content.Context;
import android.view.ViewGroup;

import com.itheima.common.ui.BaseAdapterLV;
import com.itheima.common.ui.BaseHolderLV;
import com.itheima.takeout.model.bean.ShopDetail;
import com.itheima.takeout.ui.holder.ShoppingCartHolder;

import java.util.List;

/**
 * Created by Administrator on 2017/4/26.
 *
 * @author hui
 */

public class ShoppingCartAdapter extends BaseAdapterLV<ShopDetail.CategoryBean.GoodsBean>{

    public ShoppingCartAdapter(Context context, List<ShopDetail.CategoryBean.GoodsBean> listData) {
        super(context, listData);
    }

    @Override
    public BaseHolderLV<ShopDetail.CategoryBean.GoodsBean> createViewHolder(Context context, ViewGroup parent, ShopDetail.CategoryBean.GoodsBean bean, int position) {
        return new ShoppingCartHolder(context,parent,this,position,bean);
    }
}
