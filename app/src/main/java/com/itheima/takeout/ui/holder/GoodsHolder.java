package com.itheima.takeout.ui.holder;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.itheima.common.base.Global;
import com.itheima.common.ui.BaseAdapterLV;
import com.itheima.common.ui.BaseHolderLV;
import com.itheima.takeout.R;
import com.itheima.takeout.db.greendao.CartInfo;
import com.itheima.takeout.model.bean.ShopDetail;
import com.itheima.takeout.model.dao.MyCartInfoDao;
import com.itheima.takeout.ui.activity.ShopDetailActivity;
import com.squareup.picasso.Picasso;

/**
 * Created by Administrator on 2017/4/26.
 *
 * @author hui
 */

public class GoodsHolder extends BaseHolderLV<ShopDetail.CategoryBean.GoodsBean> {


    private ImageView mIvIcon;
    private TextView mTvName;
    private TextView mTvDetail;
    private TextView mTvSaleInfo;
    private TextView mTvNewPrice;
    private TextView mTvOldPrice;
    private TextView mTvBuyCount;
    private ImageButton mIbMinus;
    private ImageButton mIbPlus;

    public GoodsHolder(Context context, ViewGroup parent, BaseAdapterLV<ShopDetail.CategoryBean.GoodsBean> adapter, int position, ShopDetail.CategoryBean.GoodsBean bean) {
        super(context, parent, adapter, position, bean);
    }

    @Override
    public View onCreateView(Context context, ViewGroup parent) {
        View item = Global.inflate(R.layout.item_shop_detail_goods, parent);
        //商品图片
        mIvIcon = (ImageView) item.findViewById(R.id.iv_icon);
        // 商品名
        mTvName = (TextView) item.findViewById(R.id.tv_name);
        // 商品信息
        mTvDetail = (TextView) item.findViewById(R.id.tv_detail);
        // 销售量
        mTvSaleInfo = (TextView) item.findViewById(R.id.tv_sale_info);

        // 新价格
        mTvNewPrice = (TextView) item.findViewById(R.id.tv_new_price);
        // 旧价格
        mTvOldPrice = (TextView) item.findViewById(R.id.tv_old_price);
        //  商品购买数量
        mTvBuyCount = (TextView) item.findViewById(R.id.tv_buy_count);
        //减号
        mIbMinus = (ImageButton) item.findViewById(R.id.ib_minus);
        //加号
        mIbPlus = (ImageButton) item.findViewById(R.id.ib_plus);
        //加号减号的点击事件
        mIbMinus.setOnClickListener(mOnClickListener);
        mIbPlus.setOnClickListener(mOnClickListener);
        return item;
    }


    @Override
    protected void onRefreshView(ShopDetail.CategoryBean.GoodsBean bean, int position) {
        Picasso.with(super.context).load(bean.getImage()).into(mIvIcon);
        mTvName.setText(bean.getName());
        mTvDetail.setText(bean.getDetail());
        String goodsInfo = "月销量 " + bean.getMonthSaleCount()
                + " 份 | 好评率 " + bean.getFeedbackRate() + "%";
        mTvSaleInfo.setText(goodsInfo);
        // 价格
        if (bean.isBargainPrice()) { // 打折
            mTvOldPrice.setVisibility(View.VISIBLE);
            mTvOldPrice.setText("￥" + bean.getOldPrice());
            mTvNewPrice.setText("￥" + bean.getNewPrice());
        } else {
            mTvOldPrice.setVisibility(View.GONE);
            mTvNewPrice.setText("￥" + bean.getNewPrice());
        }

        //购买的数量
        if (bean.mBuyCount > 0) {//有添加到购物车
            mTvBuyCount.setVisibility(View.VISIBLE);
            mIbMinus.setVisibility(View.VISIBLE);
            mTvBuyCount.setText(String.valueOf(bean.mBuyCount));

        } else {
            mTvBuyCount.setVisibility(View.GONE);
            mIbMinus.setVisibility(View.GONE);
        }


    }


    /**
     * 加号和减号的点击事件
     */
    private View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.ib_minus:
                    onBtnMinusClick();
                    break;
                case R.id.ib_plus:
                    onBtnPlusClick();//点击了加号后的动画
                    break;


            }

        }

    };

    /**
     * 点击了加号
     */
    private void onBtnPlusClick() {
        int count = ++super.bean.mBuyCount;
        mTvBuyCount.setText(String.valueOf(count));
        if (count == 1) {
            //（1）显示减号和购买数量, 动画效果
            mTvBuyCount.setVisibility(View.VISIBLE);
            mIbMinus.setVisibility(View.VISIBLE);
            //减号移动位置
            mIbMinus.setTranslationX(Global.dp2px(55));
            mTvBuyCount.setTranslationX(Global.dp2px(25));
            mIbMinus.animate().translationX(0).setDuration(200).rotation(360).start();
            mTvBuyCount.animate().translationX(0).setDuration(200).rotation(360).start();
            // （5）数据库缓存: 插入一条记录
            MyCartInfoDao cartInfoDao = ((ShopDetailActivity) super.context)
                    .getFragment1().getPresenter().getCartInfoDao();
            cartInfoDao.insertCartGoods(new CartInfo(null,bean.getId(),bean.getCategoryId(),bean.getShopId(),1));
        } else {
            // （4）数据库缓存: 商品购买数量加1
            MyCartInfoDao cartInfoDao = ((ShopDetailActivity) super.context)
                    .getFragment1().getPresenter().getCartInfoDao();
            cartInfoDao.increment(bean.getId());
        }

        // （2）刷新底部购物车布局的显示
        // （3）更新购物车的商品显示
        ((ShopDetailActivity) context).updateCartLayout();
        // （4）执行抛物线动画
        doBtnPlusAnimation();

    }

    private void doBtnPlusAnimation() {
        // 1. 动画执行的开始位置，也就是当前加号所在的窗口位置
        int[] startPosition = new int[2];
        mIbPlus.getLocationInWindow(startPosition);
        // 2. 执行点击加号的抛物线动画
        ((ShopDetailActivity) context).doBtnPlusAnimation(startPosition);
    }

    /**
     * 减号
     */
    private void onBtnMinusClick() {
        int count = --super.bean.mBuyCount;
        mTvBuyCount.setText(String.valueOf(count));
        if (count == 0) {
            Global.getMainHandler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    //（1）隐藏减号和购买数量, 动画效果
                    mTvBuyCount.setVisibility(View.GONE);
                    mIbMinus.setVisibility(View.GONE);
                }
            }, 200);
            mIbMinus.animate().translationX(Global.dp2px(55)).rotation(0).setDuration(200).start();
            mTvBuyCount.animate().translationX(Global.dp2px(25)).rotation(0).setDuration(200).start();
            //数据库缓存
            MyCartInfoDao cartInfoDao = ((ShopDetailActivity) super.context)
                    .getFragment1().getPresenter().getCartInfoDao();
            cartInfoDao.deleteCartGoods(bean.getId());

        } else {
            MyCartInfoDao cartInfoDao = ((ShopDetailActivity) super.context)
                    .getFragment1().getPresenter().getCartInfoDao();
            cartInfoDao.decrement(bean.getId());
        }
        // （3）更新购物车的商品显示
        ((ShopDetailActivity) context).updateCartLayout();
    }
}
