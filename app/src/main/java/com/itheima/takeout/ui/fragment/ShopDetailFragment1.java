package com.itheima.takeout.ui.fragment;

import android.os.Bundle;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AbsListView;

import com.itheima.common.base.BaseFragment;
import com.itheima.common.base.Const;
import com.itheima.common.util.LogUtil;
import com.itheima.takeout.R;
import com.itheima.takeout.model.bean.ShopDetail;
import com.itheima.takeout.model.bean.ShopList;
import com.itheima.takeout.model.bean.local.GoodsData;
import com.itheima.takeout.model.protocol.IHttpService;
import com.itheima.takeout.presenter.ShopDetailFragment1Presenter;
import com.itheima.takeout.ui.activity.ShopDetailActivity;
import com.itheima.takeout.ui.adapter.GoodCategoryAdapter;
import com.itheima.takeout.ui.adapter.GoodsAdapter;

import se.emilsjolander.stickylistheaders.StickyListHeadersListView;

/**
 * Created by Administrator on 2017/4/25.
 *
 * @author hui
 */

public class ShopDetailFragment1 extends BaseFragment {

    private ShopList.ShopListBean mShopListBean;
    private GoodCategoryAdapter mGoodCategoryAdapter;
    private GoodsAdapter mGoodsAdapter;
    private RecyclerView mRvGoodsType;
    private ShopDetailFragment1Presenter mPresenter;
    private StickyListHeadersListView mStickyListHeadersListView;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mShopListBean = (ShopList.ShopListBean) getArguments().getSerializable(Const.SHOP_LIST_BEAN);
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_shop_detail_01;
    }

    @Override
    public void initView() {
        mRvGoodsType = findView(R.id.rv_goods_type);
        mRvGoodsType.setLayoutManager(new LinearLayoutManager(mActivity));
        mGoodCategoryAdapter = new GoodCategoryAdapter(mActivity,null);
        mRvGoodsType.setAdapter(mGoodCategoryAdapter);
        mStickyListHeadersListView = findView(R.id.slhlv);
        mGoodsAdapter = new GoodsAdapter(mActivity,null);
        mStickyListHeadersListView.setAdapter(mGoodsAdapter);

    }

    @Override
    public void initListener() {
        mStickyListHeadersListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (mGoodsAdapter==null){
                    return;
                }
                try{
                ShopDetail.CategoryBean.GoodsBean item = mGoodsAdapter.getItem(firstVisibleItem);
                //商品类别Id
                int categoryId = item.getCategoryId();
                if (categoryId != mGoodCategoryAdapter.mSelectedBean.getId()){
                    LogUtil.i("该去改变右边的选项了" + categoryId);
                    mGoodCategoryAdapter.checkCategory(categoryId);
                }
                }catch (Exception e){
                    e.printStackTrace();
                }

            }
        });
    }

    public StickyListHeadersListView getStickyListHeadersListView() {
        return mStickyListHeadersListView;
    }

    public ShopDetailFragment1Presenter getPresenter() {
        return mPresenter;
    }

    public GoodsAdapter getGoodsAdapter(){
        return mGoodsAdapter;
    }

    @Override
    public void initData() {
        mPresenter = new ShopDetailFragment1Presenter(this);
        mPresenter.getShopDetail(mShopListBean.getId());
    }

    @Override
    public void onClick(View v, int id) {

    }

    @Override
    public void onResponse(int reqType, Message msg) {
        if (reqType== IHttpService.GET_SHOP_DETAIL){
            GoodsData goodsData = (GoodsData) msg.obj;
            mGoodCategoryAdapter.setDatas(goodsData.getCategoryBeen());
            mGoodsAdapter.setDatas(goodsData.getAllGoods());
            // 刷新底部购物车布局显示
            ((ShopDetailActivity) mActivity).updateCartLayout();
        }
    }
}
