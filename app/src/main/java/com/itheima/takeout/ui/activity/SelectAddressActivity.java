package com.itheima.takeout.ui.activity;

import android.os.Message;
import android.view.View;
import android.widget.LinearLayout;

import com.itheima.common.base.BaseActivity;
import com.itheima.takeout.R;

/**
 * Created by Administrator on 2017/4/27.
 *
 * @author hui
 */

public class SelectAddressActivity extends BaseActivity {

    private LinearLayout mLlAddAddress;

    @Override
    public int getLayoutRes() {
        return R.layout.activity_select_recepient_address;
    }

    @Override
    public void initView() {
        mLlAddAddress = findView(R.id.ll_add_address);
    }

    @Override
    public void initListener() {
        mLlAddAddress.setOnClickListener(this);
    }

    @Override
    public void initData() {

    }

    @Override
    public void onClick(View v, int id) {
        if (mLlAddAddress == v){
            //TODO 跳转到地址管理器界面
//            AddressManageActivity

        }
    }

    @Override
    public void onResponse(int reqType, Message msg) {

    }
}
