package com.itheima.takeout.ui.activity;

import android.animation.Animator;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.LinearInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.flipboard.bottomsheet.BottomSheetLayout;
import com.itheima.common.base.BaseActivity;
import com.itheima.common.base.Const;
import com.itheima.common.base.Global;
import com.itheima.takeout.R;
import com.itheima.takeout.model.bean.ShopDetail;
import com.itheima.takeout.model.bean.ShopList;
import com.itheima.takeout.model.bean.local.CartInfo;
import com.itheima.takeout.model.dao.MyCartInfoDao;
import com.itheima.takeout.ui.adapter.ShoppingCartAdapter;
import com.itheima.takeout.ui.adapter.ShopDetailPagerAdapter;
import com.itheima.takeout.ui.fragment.ShopDetailFragment1;
import com.itheima.takeout.ui.fragment.ShopDetailFragment2;
import com.itheima.takeout.ui.fragment.ShopDetailFragment3;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Administrator on 2017/4/25.
 *
 * @author hui
 */

public class ShopDetailActivity extends BaseActivity {

    private ImageButton mBtnBack;
    private FrameLayout mFlMycartZoom;
    private TextView mTvSelectCount;
    private ImageButton mIbPlus;
    private LinearLayout mLlShopInfoLayout;
    private ImageView mIvShopLogo;
    private TextView mTvShopName;
    private TextView mTvShopInfo;
    private RelativeLayout mRlTitleBar;
    private TabLayout mTabLayout;
    private ShopDetailPagerAdapter mPagerAdapter;
    private ShopDetailFragment1 fragment1;
    private LinearLayout mLlBottomCardLayout02;
    private LinearLayout mLlBottomCardLayout01;
    private TextView mTvAmount;
    private ShopList.ShopListBean mShopListBean;
    private TextView mTvSubmit;
    private View mLayoutPopCart;
    private int mColor;
    private BottomSheetLayout mBottomSheepLayout;

    @Override
    public int getLayoutRes() {
        return R.layout.activity_shop_detail;
    }

    @Override
    public void initView() {
        //标题栏
        mRlTitleBar = (RelativeLayout) findViewById(R.id.rl_title_bar);
        mBtnBack = (ImageButton) findViewById(R.id.btn_back);
        TextView tvTitle = (TextView) findViewById(R.id.tv_title);

        //选项卡和Framgnet
        mTabLayout = (TabLayout) findViewById(R.id.tab_layout);
        ViewPager viewPager = (ViewPager) findViewById(R.id.view_pager);
        mPagerAdapter = new ShopDetailPagerAdapter(getSupportFragmentManager(), null);
        viewPager.setAdapter(mPagerAdapter);
        mTabLayout.setupWithViewPager(viewPager);

        //底部购物车显示栏1 未购买东西
        mLlBottomCardLayout01 = (LinearLayout) findViewById(R.id.ll_bottom_card_layout_01);
        TextView tvSendPrice = (TextView) findViewById(R.id.tv_send_price);
        //底部购物车显示栏2  添加东西到了购物车
        mLlBottomCardLayout02 = (LinearLayout) findViewById(R.id.ll_bottom_card_layout_02);
        mTvAmount = (TextView) findViewById(R.id.tv_amount);//总金额
        TextView tvDeliveryFee = (TextView) findViewById(R.id.tv_delivery_fee); // 配送费
        //选好了
        mTvSubmit = (TextView) findViewById(R.id.tv_submit);

        //购物车
        mFlMycartZoom = (FrameLayout) findViewById(R.id.fl_mycart_zoom);
        //购物车上的条目数
        mTvSelectCount = (TextView) findViewById(R.id.tv_select_count);

        //做抛物线动画的加号
        mIbPlus = (ImageButton) findViewById(R.id.ib_plus);


        //能够弹出一个布局的自定义控件
        mBottomSheepLayout = (BottomSheetLayout) findViewById(R.id.bottom_sheep_layout);

        //商家详细信息
        mLlShopInfoLayout = (LinearLayout) findViewById(R.id.ll_shop_info_layout);
        mIvShopLogo = (ImageView) findViewById(R.id.iv_shop_logo);
        mTvShopName = (TextView) findViewById(R.id.tv_shop_name);
        mTvShopInfo = (TextView) findViewById(R.id.tv_shop_info);

    }


    @Override
    public void initData() {
        Intent intent = getIntent();
        mShopListBean = (ShopList.ShopListBean) intent.getSerializableExtra(Const.SHOP_LIST_BEAN);
        Picasso.with(this).load(mShopListBean.getImage()).into(mIvShopLogo);
        super.setPageTitle(mShopListBean.getName());
        initBackgroundColor();
        initViewPager(mShopListBean);
    }

    private void initViewPager(ShopList.ShopListBean shopListBean) {

        final String[] titles = new String[]{
                "点菜", "评价", "商家"};
        final List<Fragment> fragments = new ArrayList<>();
        fragment1 = new ShopDetailFragment1();
        Bundle bundle = new Bundle();
        bundle.putSerializable(Const.SHOP_LIST_BEAN, shopListBean);
        fragment1.setArguments(bundle);
        fragments.add(fragment1);
        fragments.add(new ShopDetailFragment2());
        fragments.add(new ShopDetailFragment3());
        mPagerAdapter.setData(fragments, titles);
        mPagerAdapter.notifyDataSetChanged();

    }

    public ShopDetailFragment1 getFragment1() {
        return fragment1;
    }

    /**
     * 刷新底部购物车控件
     */
    public void updateCartLayout() {
        CartInfo cartInfo = fragment1.getPresenter().getCartInfo();
        //底部布局的显示与隐藏
        if (cartInfo.getBuyCount()>0){
            mLlBottomCardLayout01.setVisibility(View.GONE);
            mLlBottomCardLayout02.setVisibility(View.VISIBLE);
            mFlMycartZoom.setVisibility(View.VISIBLE);

            //显示购物车数量和金额
            mTvSelectCount.setVisibility(cartInfo.getBuyCount() > 0 ? View.VISIBLE : View.GONE);
            mTvSelectCount.setText(String.valueOf(cartInfo.getBuyCount()));
            mTvAmount.setText(String.valueOf(cartInfo.getAmount()));

            // 提交按钮是否可见: 大于起送价
            boolean visible = (cartInfo.getAmount() >= mShopListBean.getSendPrice());
            if (visible){
                //大于起送价
                mTvSubmit.setText("选好了");
                mTvSubmit.setBackgroundColor(getResources()
                        .getColor(R.color.title_bar_bg));
                mTvSubmit.setEnabled(true);
            }else{
                float less = mShopListBean.getSendPrice() - cartInfo.getAmount();
                mTvSubmit.setText("还差"+ less +"元起送");
                mTvSubmit.setBackgroundColor(getResources()
                        .getColor(R.color.shop_detail_my_cart_bg));
                mTvSubmit.setEnabled(false);
            }


        }else{
            mLlBottomCardLayout01.setVisibility(View.VISIBLE);
            mLlBottomCardLayout02.setVisibility(View.GONE);
            mFlMycartZoom.setVisibility(View.GONE);
        }


    }


    /**
     * 初始化标题栏,颜色的变化.
     */
    private void initBackgroundColor() {
        int colorID = R.color.shop_detail_bg_01 + new Random().nextInt(4);
        mColor = getResources().getColor(colorID);
        mRlTitleBar.setBackgroundColor(mColor);
    }

    @Override
    public void initListener() {
        mBtnBack.setOnClickListener(this);

        mLlBottomCardLayout02.setOnClickListener(this);
        mTvSubmit.setOnClickListener(this);
    }

    @Override
    public void onClick(View v, int id) {
        switch (id) {
            case R.id.btn_back:
                finish();
                break;
            case R.id.ib_plus:
                onBtnPlusClick();
                break;
            case R.id.ll_bottom_card_layout_02:
                showOrHideCartPopLayout();
                break;
            case R.id.tv_submit:
                Intent intent = new Intent(ShopDetailActivity.this,OrderConfirmActivity.class);
                startActivity(intent);
                break;



        }
    }

    /**
     * 显示底部购物车弹窗
     */
    private void showOrHideCartPopLayout() {
        mLayoutPopCart = Global.inflate(R.layout.layout_pop_cart);
        View llPopTitleBar = mLayoutPopCart.findViewById(R.id.ll_pop_title_bar);
        llPopTitleBar.setBackgroundColor(mColor);
        // 点击清空购物车
        TextView tvClear = (TextView) mLayoutPopCart.findViewById(R.id.tv_clear_cart);
        tvClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearShoppingCart();        // 清空购物车
            }


        });

        if (mBottomSheepLayout.isSheetShowing()) {
            // 显示 -> 关闭
            mBottomSheepLayout.dismissSheet();
        } else {    // 关闭 -> 显示
            // 显示列表
            ListView listView = (ListView) mLayoutPopCart.findViewById(R.id.list_view);
            List<ShopDetail.CategoryBean.GoodsBean> listDatas =
                    fragment1.getPresenter().getAllCartData();
            listView.setAdapter(new ShoppingCartAdapter(this, listDatas));

            // 显示弹出窗
            mBottomSheepLayout.showWithSheetView(mLayoutPopCart);
        }


    }

    /**
     * 关闭底部弹窗
     */
    public  void hideBottomSheetLayout(){
        mBottomSheepLayout.dismissSheet();
    }

    /**
     * 清空购物车上的数据
     */
    private void clearShoppingCart() {
        super.showDialog("提示", "确定要清空购物车中所有的商品吗", new OnDialogClickListener() {
            @Override
            public void onConfirm(DialogInterface dialog) {
                // 1. 刷新购物车列表项显示
                hideBottomSheetLayout();
                // 2. 刷新右侧列表对应列表项显示
                fragment1.getPresenter().clearCartDatas();
                fragment1.getGoodsAdapter().notifyDataSetChanged();
                // 3. 刷新购物车的支付总额和数量
                updateCartLayout();

                //4. 清除数据库缓存
//                fragment1.getPresenter();
                MyCartInfoDao cartInfoDao = fragment1.getPresenter().getCartInfoDao();
                //清除这个店的数据
                cartInfoDao.clearCartGoods(mShopListBean.getId());

            }

            @Override
            public void onCancel(DialogInterface dialog) {

            }
        });

    }

    /**
     * 加号执行抛物线动画
     * @param startPosition 加号开始位置的x和y坐标的数组
     */
  public void  doBtnPlusAnimation(int[] startPosition){
      // 需要执行抛物线动画时需要先显示，否则隐藏。
      mIbPlus.setVisibility(View.VISIBLE);
      // 动画开始位置
      mIbPlus.setTranslationX(startPosition[0]);
      // 减去状态栏的高度：24dp
      mIbPlus.setTranslationY(startPosition[1] - Global.dp2px(24));

      // 动画结束位置
      int[] endPosition = new int[2];
      mFlMycartZoom.getLocationInWindow(endPosition);
      // 垂直往下移动的加速动画
      mIbPlus.animate()
              .setDuration(400)
              .setInterpolator(new AccelerateInterpolator())
              .translationY(endPosition[1])
              .start();

      // 水平往左移动的匀速动画
      mIbPlus.animate()
              .setDuration(400)
              .setInterpolator(new LinearInterpolator())
              .translationX(endPosition[0])
              .setListener(new Animator.AnimatorListener() {
                  @Override
                  public void onAnimationStart(Animator animation) {
                  }

                  @Override
                  public void onAnimationEnd(Animator animation) {
                      // 隐藏加号按钮
                      mIbPlus.setVisibility(View.GONE);
                      // 执行缩放购物车的缩放动画
                      doCartViewAnimation();

                  }

                  @Override
                  public void onAnimationCancel(Animator animation) {
                  }

                  @Override
                  public void onAnimationRepeat(Animator animation) {
                  }
              }).start();


    }

    /**
     * 执行缩放购物车的缩放动画
     */
    private void doCartViewAnimation() {
        // 先执行放大动画
        mFlMycartZoom.animate().scaleX(1.1f).scaleY(1.1f).setDuration(200).start();
        // 再执行缩小动画
        Global.getMainHandler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mFlMycartZoom.animate().scaleX(1f).scaleY(1f).setDuration(200).start();
            }
        }, 200);
    }

    private void onBtnPlusClick() {


    }

    @Override
    public void onResponse(int reqType, Message msg) {

    }
}
