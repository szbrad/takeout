package com.itheima.takeout.ui.adapter;

import android.content.Context;
import android.view.ViewGroup;

import com.itheima.common.ui.BaseAdapterRV;
import com.itheima.common.ui.BaseHolderRV;
import com.itheima.takeout.model.bean.ShopDetail;
import com.itheima.takeout.ui.holder.GoodCategoryHolder;

import java.util.List;

/**
 * Created by Administrator on 2017/4/26.
 *
 * @author hui
 */

public class GoodCategoryAdapter extends BaseAdapterRV<ShopDetail.CategoryBean>{

    public ShopDetail.CategoryBean mSelectedBean;

    public GoodCategoryAdapter(Context context, List<ShopDetail.CategoryBean> listData) {
        super(context, listData);

    }

    @Override
    public BaseHolderRV createViewHolder(Context context, ViewGroup parent, int viewType) {
        return new GoodCategoryHolder(context,parent,this,viewType);
    }

    @Override
    public void setDatas(List<ShopDetail.CategoryBean> newData) {
        super.setDatas(newData);
        mSelectedBean = newData.get(0);
    }
    public void checkCategory(int categoryId){

        for (int i = 0,size = listData.size(); i < size; i++) {
            ShopDetail.CategoryBean categoryBean = listData.get(i);
            int id = categoryBean.getId();
            if (categoryId == id){
                //当商家id类别相同的时候,改变当前选中项
                mSelectedBean = listData.get(i);
                //刷新列表
                notifyDataSetChanged();
                return;
            }
        }

    }
}
