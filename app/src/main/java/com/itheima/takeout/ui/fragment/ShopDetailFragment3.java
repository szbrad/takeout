package com.itheima.takeout.ui.fragment;

import android.os.Message;
import android.view.View;

import com.itheima.common.base.BaseFragment;
import com.itheima.takeout.R;

/**
 * Created by Administrator on 2017/4/25.
 *
 * @author hui
 */

public class ShopDetailFragment3 extends BaseFragment {
    @Override
    public int getLayoutRes() {
        return R.layout.fragment_shop_detail_03;
    }

    @Override
    public void initView() {

    }

    @Override
    public void initListener() {

    }

    @Override
    public void initData() {

    }

    @Override
    public void onClick(View v, int id) {

    }

    @Override
    public void onResponse(int reqType, Message msg) {

    }
}
