package com.itheima.takeout.ui.adapter;

import android.content.Context;
import android.view.ViewGroup;

import com.itheima.common.ui.BaseAdapterRV;
import com.itheima.common.ui.BaseHolderRV;
import com.itheima.takeout.model.bean.HomeBean;
import com.itheima.takeout.ui.holder.HomePromotionTypesHolder;

import java.util.List;

/**
 * Created by Administrator on 2017/4/22.
 *
 * @author hui
 */

public class HomePromotionTypesAdapter extends BaseAdapterRV<HomeBean.PromotionTypesBean> {

    public HomePromotionTypesAdapter(Context context, List<HomeBean.PromotionTypesBean> listData) {
        super(context, listData);
    }

    @Override
    public BaseHolderRV<HomeBean.PromotionTypesBean> createViewHolder(Context context, ViewGroup parent, int viewType) {
        return new HomePromotionTypesHolder(context,parent,this,viewType);
    }
}
