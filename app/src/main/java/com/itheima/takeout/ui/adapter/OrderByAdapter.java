package com.itheima.takeout.ui.adapter;

import android.content.Context;
import android.view.ViewGroup;

import com.itheima.common.ui.BaseAdapterLV;
import com.itheima.common.ui.BaseHolderLV;
import com.itheima.takeout.model.bean.OrderBy;
import com.itheima.takeout.ui.holder.OrderByHolder;

import java.util.List;

/**
 * Created by Administrator on 2017/4/25.
 *
 * @author hui
 */

public class OrderByAdapter  extends BaseAdapterLV<OrderBy.OrderByListBean>{

    public OrderBy.OrderByListBean mSelectOrderBy;

    public OrderByAdapter(Context context, List<OrderBy.OrderByListBean> listData) {
        super(context, listData);
    }
    @Override
    public BaseHolderLV<OrderBy.OrderByListBean> createViewHolder(Context context, ViewGroup parent, OrderBy.OrderByListBean bean, int position) {
        return new OrderByHolder(context,parent,this,position,bean);
    }
}
