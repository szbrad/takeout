package com.itheima.takeout.ui.holder;

import android.content.Context;
import android.content.res.Resources;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.itheima.common.base.Global;
import com.itheima.common.ui.BaseAdapterLV;
import com.itheima.common.ui.BaseHolderLV;
import com.itheima.takeout.R;
import com.itheima.takeout.model.bean.ShopCategory;
import com.itheima.takeout.ui.adapter.ShopCategoryParentAdapter;

/**
 * Created by Administrator on 2017/4/24.
 *
 * @author hui
 */

public class ShopCategoryParentHolder extends BaseHolderLV<ShopCategory.CategoryListBean> {

    private TextView mTvTitle;
    private TextView mTvShopCount;
    private ImageView mIvArrowRight;

    public ShopCategoryParentHolder(Context context, ViewGroup parent, BaseAdapterLV<ShopCategory.CategoryListBean> adapter, int position, ShopCategory.CategoryListBean bean) {
        super(context, parent, adapter, position, bean);
    }


    @Override
    public View onCreateView(Context context, ViewGroup parent) {
        View categoryView = Global.inflate(R.layout.item_shop_category);
        //商家名字
        mTvTitle = (TextView) categoryView.findViewById(R.id.tv_title);
        //商家个数
        mTvShopCount = (TextView) categoryView.findViewById(R.id.tv_shop_count);
        //向右指示的箭头
        mIvArrowRight = (ImageView) categoryView.findViewById(R.id.iv_arrow_right);

        return categoryView;
    }

    @Override
    protected void onRefreshView(ShopCategory.CategoryListBean bean, int position) {
        //判断商家是否有子商家
        if (bean.childrenCategory != null && bean.childrenCategory.size() > 0) {
            //如果有,就显示向右的箭头
            mIvArrowRight.setVisibility(View.VISIBLE);
        } else {
            //如果没有,就隐藏掉
            mIvArrowRight.setVisibility(View.GONE);
        }
        //设置标题
        mTvTitle.setText(bean.getName());
        mTvShopCount.setText("(" + bean.getShopCount() +")");

        // 高亮显示商家类别
        Resources resources = context.getResources();
        // 设置字体是否高亮显示
        ShopCategory.CategoryListBean selectedBean =
                ((ShopCategoryParentAdapter) adapter).mSelectCategory;

        if (selectedBean != null && (selectedBean.getId() == bean.getId())) {
            mTvTitle.setTextColor(resources.getColor(R.color.shop_category_item_selected));
            mTvShopCount.setTextColor(resources.getColor(R.color.shop_category_item_selected));
            mIvArrowRight.setBackgroundResource(R.drawable.arror_selected);
        } else {
            mTvTitle.setTextColor(resources.getColor(R.color.list_item_text_01));
            mTvShopCount.setTextColor(resources.getColor(R.color.list_item_text_02));
            mIvArrowRight.setBackgroundResource(R.drawable.arror_unselected);
        }

    }

}
