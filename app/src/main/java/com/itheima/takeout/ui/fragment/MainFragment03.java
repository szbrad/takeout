package com.itheima.takeout.ui.fragment;

import android.os.Message;
import android.view.View;

import com.itheima.common.base.BaseFragment;
import com.itheima.takeout.R;

/**
 * Created by Administrator on 2017/4/22.
 *
 * @author hui
 */

public class MainFragment03 extends BaseFragment {
    @Override
    public int getLayoutRes() {
        return R.layout.fragment_03;
    }

    @Override
    public void initView() {

    }

    @Override
    public void initListener() {

    }

    @Override
    public void initData() {

    }

    @Override
    public void onClick(View v, int id) {

    }

    @Override
    public void onResponse(int reqType, Message msg) {

    }
}
