package com.itheima.takeout.ui.holder;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.itheima.common.base.Global;
import com.itheima.common.ui.BaseAdapterLV;
import com.itheima.common.ui.BaseHolderLV;
import com.itheima.common.util.Utils;
import com.itheima.takeout.R;
import com.itheima.takeout.model.bean.ShopDetail;
import com.itheima.takeout.model.dao.MyCartInfoDao;
import com.itheima.takeout.ui.activity.ShopDetailActivity;
import com.itheima.takeout.ui.fragment.ShopDetailFragment1;

/**
 * Created by Administrator on 2017/4/26.
 *
 * @author hui
 */

public class ShoppingCartHolder extends BaseHolderLV<ShopDetail.CategoryBean.GoodsBean> {

    private TextView mTvName;
    private TextView mTvTypeAllPrice;
    private TextView mTvCount;

    public ShoppingCartHolder(Context context, ViewGroup parent, BaseAdapterLV<ShopDetail.CategoryBean.GoodsBean> adapter, int position, ShopDetail.CategoryBean.GoodsBean bean) {
        super(context, parent, adapter, position, bean);
    }

    @Override
    public View onCreateView(Context context, ViewGroup parent) {
        View item = Global.inflate(R.layout.item_shopping_cart, parent);
        mTvName = (TextView) item.findViewById(R.id.tv_name);
        mTvTypeAllPrice = (TextView) item.findViewById(R.id.tv_type_all_price);
        LinearLayout ll = (LinearLayout) item.findViewById(R.id.ll);
        ImageButton ibMinus = (ImageButton) item.findViewById(R.id.ib_minus);
        mTvCount = (TextView) item.findViewById(R.id.tv_cart_goods_amount);
        ImageButton ibAdd = (ImageButton) item.findViewById(R.id.ib_plus);

        ibMinus.setOnClickListener(mOnClickListener);
        ibAdd.setOnClickListener(mOnClickListener);

        return item;
    }

    @Override
    protected void onRefreshView(ShopDetail.CategoryBean.GoodsBean bean, int position) {
        mTvName.setText(bean.getName());
        mTvCount.setText(bean.mBuyCount + "");
        double price = bean.getNewPrice() * bean.mBuyCount;
        mTvTypeAllPrice.setText(Utils.formatAmount(price));

    }

    private View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.ib_minus:      // 减号
                    onClickMinusBtn();
                    break;
                case R.id.ib_plus:       // 加号
                    onClickAddBtn();
                    break;
            }
        }


    };

    /**
     * 加号监听
     */
    private void onClickAddBtn() {
        //刷新列表显示
        super.bean.mBuyCount++;
        super.adapter.notifyDataSetChanged();

        ShopDetailFragment1 fragment1 = ((ShopDetailActivity) context).getFragment1();
        fragment1.getGoodsAdapter().notifyDataSetChanged();
        ((ShopDetailActivity) context).updateCartLayout();

        MyCartInfoDao cartInfoDao =  ((ShopDetailActivity) super.context)
                .getFragment1().getPresenter().getCartInfoDao();
        cartInfoDao.increment(bean.getId());
    }

    /**
     * 减号监听
     */
    private void onClickMinusBtn() {
        // 1. 刷新购物车列表项显示
        super.bean.mBuyCount--;
        super.adapter.notifyDataSetChanged();

        // 2. 刷新右侧列表对应列表项显示
        ((ShopDetailActivity) context).getFragment1()
                .getGoodsAdapter().notifyDataSetChanged();

        // 3. 刷新购物车的支付总额和数量
        ((ShopDetailActivity) context).updateCartLayout();

        // 4. 数量为1时，点击减号会把整一行删除掉
        if (super.bean.mBuyCount == 0) {
            // 删除购物车列表中的一个列表项
            super.adapter.remove(super.bean);

            // 列表中没有购买的商品，隐藏弹出窗口
            if (super.adapter.getCount() < 1) {
                ((ShopDetailActivity) context).hideBottomSheetLayout();
            }
            //TODO 同步数据库

            MyCartInfoDao cartInfoDao =  ((ShopDetailActivity) super.context)
                    .getFragment1().getPresenter().getCartInfoDao();
            cartInfoDao.decrement(bean.getId());

        }
    }

}
