package com.itheima.takeout.ui.holder;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.itheima.common.base.Global;
import com.itheima.common.ui.BaseAdapterRV;
import com.itheima.common.ui.BaseHolderRV;
import com.itheima.common.util.Utils;
import com.itheima.takeout.R;
import com.itheima.takeout.model.bean.HomeBean;
import com.squareup.picasso.Picasso;

/**
 * Created by Administrator on 2017/4/22.
 *
 * @author hui
 */

public class HomePromotionTypesHolder extends BaseHolderRV<HomeBean.PromotionTypesBean> {

    private ImageView mIvIcon;
    private TextView mTvTitle;

    public HomePromotionTypesHolder(Context context, ViewGroup parent, BaseAdapterRV<HomeBean.PromotionTypesBean> adapter, int itemType) {
        super(context, parent, adapter, itemType, R.layout.item_grid);
    }

    @Override
    public void onFindViews(View itemView) {
        mIvIcon = (ImageView) itemView.findViewById(R.id.iv_icon);
        mTvTitle = (TextView) itemView.findViewById(R.id.tv_title);
        //设置网格的宽度
        RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) itemView.getLayoutParams();
        params.width = (int) (Global.mScreenWidth/5);
        itemView.setLayoutParams(params);
    }

    @Override
    protected void onRefreshView(HomeBean.PromotionTypesBean bean, int position) {
        String imgUrl = Utils.replaceIp(bean.getIcon());
        Picasso.with(super.context).load(imgUrl).into(mIvIcon);
        String name = bean.getName();
        mTvTitle.setText(name);

    }
}
