package com.itheima.takeout.ui.holder;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.itheima.common.base.Const;
import com.itheima.common.base.Global;
import com.itheima.common.ui.BaseAdapterRV;
import com.itheima.common.ui.BaseHolderRV;
import com.itheima.common.util.LogUtil;
import com.itheima.common.util.Utils;
import com.itheima.takeout.R;
import com.itheima.takeout.model.bean.ShopList;
import com.itheima.takeout.ui.activity.ShopDetailActivity;
import com.squareup.picasso.Picasso;

import java.util.List;

import static com.itheima.common.util.Utils.replaceIp;

/**
 * Created by Administrator on 2017/4/21.
 *
 * @author hui
 */

public class ShopBeanHolder extends BaseHolderRV<ShopList.ShopListBean> {


    private LinearLayout mLlActivityList;
    private RelativeLayout mRlActivityLayout;
    private TextView mActivityCount;
    private ImageView mIvGoodsImage;
    private TextView mTvCartGoodsAmount;
    private ImageView mIvIsNewShop;
    private TextView mTvTitle;
    private RatingBar mRatingBar;
    private TextView mTvMonthSale;
    private TextView mTvSendPrice;
    private TextView mTvDistance;

    public ShopBeanHolder(Context context, ViewGroup parent, BaseAdapterRV adapter, int itemType) {
        super(context, parent, adapter, itemType, R.layout.item_home_shop);
    }

    @Override
    public void onFindViews(View itemView) {

        //是否是新的商家
        mIvIsNewShop = (ImageView) itemView.findViewById(R.id.iv_is_new_shop);
        //添加到购物车的数量
        mTvCartGoodsAmount = (TextView) itemView.findViewById(R.id.tv_cart_goods_amount);
        mIvGoodsImage = (ImageView) itemView.findViewById(R.id.iv_goods_image);
        mTvTitle = (TextView) itemView.findViewById(R.id.tv_title);

        //商家评分
        mRatingBar = (RatingBar) itemView.findViewById(R.id.rating_bar);
        //销售单数
        mTvMonthSale = (TextView) itemView.findViewById(R.id.tv_month_sale);
        //配送金额
        mTvSendPrice = (TextView) itemView.findViewById(R.id.tv_send_price);
        //距离
        mTvDistance = (TextView) itemView.findViewById(R.id.tv_distance);
        //活动个数
        mActivityCount = (TextView) itemView.findViewById(R.id.tv_activity_count);
        //活动的父控件
        mRlActivityLayout = (RelativeLayout) itemView.findViewById(R.id.rl_activity_layout);
        //活动
        mLlActivityList = (LinearLayout) itemView.findViewById(R.id.ll_activity_list);

        //对活动添加点击事件,控制活动的隐藏和显示
        mRlActivityLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final ViewGroup.LayoutParams params = mLlActivityList.getLayoutParams();

                if (bean.isExpanded) {
                    //展开---->隐藏
                    bean.isExpanded = false;
                    int startHeight = params.height;
                    int endHeight = 2 * activityHeight;
                    LogUtil.i("点击了 现在要隐藏");
                    changActivityHeight(params, startHeight, endHeight);
                } else {
                    //隐藏到展开
                    bean.isExpanded = true;
                    int endHeight = bean.getActivityList().size() * activityHeight;
                    int startHeight = 2 * activityHeight;
                    LogUtil.i("点击了  现在要显示");
                    changActivityHeight(params, startHeight, endHeight);
                }

            }
        });
    }

    @Override
    protected void onRefreshView(final ShopList.ShopListBean bean, int position) {
        //添加到购物车的数量
        mTvCartGoodsAmount.setVisibility(View.GONE);
        //店名
        mTvTitle.setText(bean.getName());
        //商家图片
        String imgUrl = replaceIp(bean.getImage());
        Picasso.with(super.context).load(imgUrl).into(mIvGoodsImage);
        //销售单数
        mTvMonthSale.setText(String.valueOf(bean.getMonthSaleCount()));
        //判断是否是新店
        if (bean.getTag() == 1) {
            mIvIsNewShop.setVisibility(View.VISIBLE);
        } else {
            mIvIsNewShop.setVisibility(View.GONE);
        }
        //商家评分
        mRatingBar.setRating((float) bean.getScore());
        //月销量
        mTvMonthSale.setText(String.valueOf("月销:" + bean.getMonthSaleCount() + "份"));
        //起送价和配送费显示：￥0起送/配送费￥9
        String sendPricebean = bean.getDeliveryFee() == 0 ? "免费配送" : "配送￥:" + bean.getScore() + "起";
        mTvSendPrice.setText("起送￥:" + bean.getSendPrice() + "|" + sendPricebean);
        //显示距离
        String distance = bean.getDistance() > 1000 ? bean.getDeliveryFee() / 1000 + "km" : bean.getDistance() + "m";
        mTvDistance.setText(distance);
        //显示商家活动
        showShopActivity(bean);


    }

    @Override
    protected void onItemClick(View itemView, int position, ShopList.ShopListBean bean) {
        super.onItemClick(itemView, position, bean);
        Intent intent = new Intent(context, ShopDetailActivity.class);
        intent.putExtra(Const.SHOP_LIST_BEAN,bean);
        context.startActivity(intent);


    }

    /**
     * 改变活动的高度
     *
     * @param params
     * @param startHeight 开始高度
     * @param endHeight   结束高度
     */
    private void changActivityHeight(final ViewGroup.LayoutParams params, int startHeight, int endHeight) {
        ValueAnimator animator = ValueAnimator.ofInt(startHeight, endHeight);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                //获取百分比
                int height = (int) animation.getAnimatedValue();
                params.height = height;
                mLlActivityList.setLayoutParams(params);
            }
        });
        animator.setDuration(400);
        animator.start();


    }

    private int activityHeight = Global.dp2px(20);


    private void showShopActivity(ShopList.ShopListBean bean) {
        List<ShopList.ShopListBean.ActivityListBean> activityList = bean.getActivityList();
        //判断是否商家是否有活动
        boolean hasShopActivity = (activityList != null && activityList.size() > 0);
        if (hasShopActivity) {
            //获取活动个数
            int size = activityList.size();
            //有商家活动
            mRlActivityLayout.setVisibility(View.VISIBLE);
            ViewGroup.LayoutParams params = mLlActivityList.getLayoutParams();
            if (size > 2) {
                params.height = bean.isExpanded ? size * activityHeight : 2 * activityHeight;
                //当活动的个数大于两个的时候显示活动个数
                mActivityCount.setVisibility(View.VISIBLE);
                mActivityCount.setText(size + "个活动");
            } else {
                params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
                mActivityCount.setVisibility(View.GONE);
            }
            mLlActivityList.setLayoutParams(params);
            //删除之前的所有活动
            mLlActivityList.removeAllViews();
            //添加活动
            for (int i = 0; i < size; i++) {
                LinearLayout item = createActivityItem(activityList, size, i);
                mLlActivityList.addView(item);
            }


        } else {
            //没有商家活动
            mRlActivityLayout.setVisibility(View.GONE);
        }


    }

    @NonNull
    private LinearLayout createActivityItem(List<ShopList.ShopListBean.ActivityListBean> activityList, int size, int i) {
        //创建父控件
        LinearLayout item = new LinearLayout(super.context);
        item.setOrientation(LinearLayout.HORIZONTAL);
        item.setGravity(Gravity.CENTER_HORIZONTAL);

        //创建图片
        int ivHeight = Global.dp2px(15);//获取图片的高
        ImageView iv = new ImageView(super.context);
        String imgUrl = Utils.replaceIp(activityList.get(i).getIcon());
        Picasso.with(super.context).load(imgUrl).into(iv);
        LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(ivHeight, ivHeight);
        params1.rightMargin = Global.dp2px(5);
        item.addView(iv, params1);
        //创建标题
        TextView tvTitle = new TextView(super.context);
        String title1 = activityList.get(i).getTitle();
        tvTitle.setTextColor(Color.GRAY);
        tvTitle.setTextSize(11);
        tvTitle.setMaxLines(1);
        tvTitle.setEllipsize(TextUtils.TruncateAt.END);
        tvTitle.setText(title1);
        item.addView(tvTitle);
        item.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, activityHeight));
        return item;
    }


}
