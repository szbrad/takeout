package com.itheima.takeout.ui.holder;

import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.itheima.common.base.Global;
import com.itheima.common.ui.BaseAdapterRV;
import com.itheima.common.ui.BaseHolderRV;
import com.itheima.common.util.Utils;
import com.itheima.takeout.R;
import com.itheima.takeout.model.bean.HomeBean;
import com.itheima.takeout.ui.adapter.HomePromotionTypesAdapter;

import java.util.List;

/**
 * Created by Administrator on 2017/4/21.
 *
 * @author hui
 */

public class HomeBeanHolder extends BaseHolderRV<HomeBean> {


    private SliderLayout mSliderLayout;
    private RecyclerView mRecyclerView;
    private ViewFlipper mViewFlipper;
    private HomePromotionTypesAdapter mAdapter;

    public HomeBeanHolder(Context context, ViewGroup parent, BaseAdapterRV adapter, int itemType) {
        super(context, parent, adapter, itemType, R.layout.item_home_header);
    }

    @Override
    public void onFindViews(View itemView) {
        mSliderLayout = (SliderLayout) itemView.findViewById(R.id.slider_layout);
        mRecyclerView = (RecyclerView) itemView.findViewById(R.id.recycler_view_02);
        //热榜
        mViewFlipper = (ViewFlipper) itemView.findViewById(R.id.view_flipper);

        mViewFlipper.setInAnimation(AnimationUtils.loadAnimation(super.context, R.anim.push_up_in));
        mViewFlipper.setOutAnimation(AnimationUtils.loadAnimation(super.context, R.anim.push_up_out));
        mViewFlipper.startFlipping();


        //设置推荐类型的布局管理起  设置为两排 横向布局,不到最后面
        mRecyclerView.setLayoutManager(new GridLayoutManager(super.context, 2, GridLayoutManager.HORIZONTAL, false));
        mAdapter = new HomePromotionTypesAdapter(super.context, null);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    protected void onRefreshView(HomeBean bean, int position) {

        //初始化SliderLayout  轮播图
        initSliderLayout(bean);

        //初始化热销榜

        initHotSale(bean.getHotSaleList());

        List<HomeBean.PromotionTypesBean> promotionTypes = bean.getPromotionTypes();
        mAdapter.setDatas(promotionTypes);
        List<HomeBean.HotSaleListBean> hotSaleList = bean.getHotSaleList();

    }

    /**
     * 热销榜的初始化
     *
     * @param hotSaleList
     */
    private void initHotSale(List<HomeBean.HotSaleListBean> hotSaleList) {
        //删除之前的所有子view
        mViewFlipper.removeAllViews();

        for (int i = 0, size = hotSaleList.size(); i < size; i++) {
            TextView hotSale = (TextView) Global.inflate(R.layout.item_view_flipper);
            hotSale.setText(hotSaleList.get(i).getTitle());
            hotSale.setTag(hotSaleList.get(i));
            mViewFlipper.addView(hotSale);
        }
        mViewFlipper.startFlipping();   


    }

    /**
     * 初始化轮播图
     *
     * @param bean HomeBean
     */
    private void initSliderLayout(HomeBean bean) {
        List<HomeBean.PromotionListBean> promotionList = bean.getPromotionList();
        for (int i = 0, size = promotionList.size(); i < size; i++) {
            HomeBean.PromotionListBean promotionListBean = promotionList.get(i);
            TextSliderView sliderView = new TextSliderView(super.context);
            String imgUrl = Utils.replaceIp(promotionListBean.getImage());
            sliderView.description(promotionListBean.getDetail()).image(imgUrl);
            mSliderLayout.addSlider(sliderView);
        }
    }
}
