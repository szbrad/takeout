package com.itheima.takeout.ui.activity;

import android.graphics.Color;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.LinearLayout;

import com.itheima.common.base.BaseActivity;
import com.itheima.common.base.Global;
import com.itheima.common.ui.GradientTab;
import com.itheima.takeout.R;
import com.itheima.takeout.ui.adapter.MyFragmentAdapter;
import com.itheima.takeout.ui.fragment.MainFragment01;
import com.itheima.takeout.ui.fragment.MainFragment02;
import com.itheima.takeout.ui.fragment.MainFragment03;
import com.itheima.takeout.ui.fragment.MainFragment04;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends BaseActivity {

    private int[] icons = new int[]{
            R.drawable.icon_tab_01,
            R.drawable.icon_tab_02,
            R.drawable.icon_tab_03,
            R.drawable.icon_tab_04,
    };

    private int[] iconsSelected = new int[]{
            R.drawable.icon_tab_01_selected,
            R.drawable.icon_tab_02_selected,
            R.drawable.icon_tab_03_selected,
            R.drawable.icon_tab_04_selected,
    };

    private String[] titles = new String[]{
            "消息", "吃啥", "订单", "我的"
    };
    //选项卡
    private GradientTab[] mTab = new GradientTab[4];
    //当前选中的选项卡
    private GradientTab mCurrentTab;
    //选项卡的父控件
    private LinearLayout mTabLayout;
    private ViewPager mVp;
    @Override
    public int getLayoutRes() {
        return R.layout.activity_main;
    }

    @Override
    public void initView() {
        initTabs();
        initViewPager();
    }

    private void initViewPager() {
        mVp = findView(R.id.viewpager);
        List<Fragment> fragments = new ArrayList<>();
        fragments.add(new MainFragment01());
        fragments.add(new MainFragment02());
        fragments.add(new MainFragment03());
        fragments.add(new MainFragment04());
        mVp.setAdapter(new MyFragmentAdapter(getSupportFragmentManager(),fragments));

    }

    /**
     * 初始化选项卡
     */
    private void initTabs() {
        mTabLayout = findView(R.id.ll_tab_layout);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT);
        params.weight = 1; //设置宽度的权重
        int padding = Global.dp2px(6);
        for (int i = 0, length = titles.length; i < length; i++) {
            GradientTab tab = new GradientTab(MainActivity.this);
            mTab[i] = tab;
            tab.setTag(i);
            tab.setPadding(padding, padding, padding, padding);
            // 设置标题与图标
            tab.setHighlightColor(Color.parseColor("#ffFF2D4B"));
            tab.setTextAndIcon(titles[i], icons[i], iconsSelected[i]);
            mTabLayout.addView(tab, params);
            tab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //点击的是第几个选项卡
                    int position = (int) v.getTag();
                    //让选项卡和ViewPager关联起来
                    mVp.setCurrentItem(position);

                }
            });
        }
        //设置默认的选中卡
        mCurrentTab = mTab[0];
        mCurrentTab.setTabSelected(true);
    }

    @Override
    public void initListener() {
        mVp.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                if (position!=titles.length-1){
                    //要实现渐变的两个选项卡
                    GradientTab leftTab = mTab[position];
                    GradientTab rightTab = mTab[position + 1];

                    leftTab.updateTabAlpha(1-positionOffset);
                    rightTab.updateTabAlpha(positionOffset);
                }

            }
            @Override
            public void onPageSelected(int position) {
                //将上一个选中卡取消选中状态
                mCurrentTab.setTabSelected(false);
                //将当前选中的选项卡赋值个当前选中卡
                mCurrentTab = mTab[position];
                //设置当前选中卡为选中状态
                mCurrentTab.setTabSelected(true);
            }
            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    public void initData() {

    }

    @Override
    public void onClick(View v, int id) {

    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onResponse(int reqType, Message msg) {

    }
}
