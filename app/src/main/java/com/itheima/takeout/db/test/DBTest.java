package com.itheima.takeout.db.test;

import com.itheima.common.util.LogUtil;
import com.itheima.takeout.db.greendao.CartInfo;
import com.itheima.takeout.model.dao.MyCartInfoDao;

import java.util.List;

/**
 * Created by Administrator on 2017/4/27.
 *
 * @author hui
 */

public class DBTest {

    MyCartInfoDao mCartInfoDao;
    public void test(){
        mCartInfoDao = new MyCartInfoDao();
        mCartInfoDao.clearAll();
        LogUtil.i("---------增--------------");
        mCartInfoDao.insertCartGoods(new CartInfo(null, 10, 1, 1, 2));
        mCartInfoDao.insertCartGoods(new CartInfo(null, 11, 1, 1, 2));
        mCartInfoDao.insertCartGoods(new CartInfo(null, 12, 1, 1, 2));
        printAll();
        LogUtil.i("---------删--------------");
        mCartInfoDao.deleteCartGoods(10);
        printAll();
        LogUtil.i("---------改--------------");
        mCartInfoDao.increment(11);//购买数量加1
        mCartInfoDao.decrement(12); //购买数量减1
        printAll();

        LogUtil.i("---------清空--------------");
        mCartInfoDao.clearAll();

        LogUtil.i("---------查--------------");

        printAll();
    }

    private void printAll(){
        List<CartInfo> cartInfos = mCartInfoDao.queryAll();
        if (cartInfos!=null && cartInfos.size()>0){
            for (CartInfo bean : cartInfos) {
                LogUtil.d("-----" + bean);
            }
        }else{
            LogUtil.d("--数据库表中没有数据了---");
        }


    }

}
