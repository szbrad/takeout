package com.itheima.takeout.db.test;

import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Schema;

/**
 * Created by Administrator on 2017/4/26.
 *
 * @author hui
 */

public class GreenDaoTest {

    public static void main(String[] args) {

        Schema schema = new Schema(1, "com.itheima.takeout.db.greendao");
        //添加一张表
        Entity entity = schema.addEntity("CartInfo");
        entity.addIdProperty().autoincrement();             //添加一个自增id
        entity.addIntProperty("goodsId");                    //商品id
        entity.addIntProperty("categoryId");                    //商品类别id
        entity.addIntProperty("shopId");                   //商家id
        entity.addIntProperty("count");                 //购买数量

        //创建表2
        Entity address = schema.addEntity("Address");
        address.addIdProperty().autoincrement();                    //自增id
        address.addStringProperty("username");              //用户名
        address.addIntProperty("sex");                      //性别
        address.addStringProperty("phone");                 //电话
        address.addStringProperty("addressName");      //地址名称
        address.addStringProperty("addressDetail");    //地址详情
        address.addDoubleProperty("latitude");              //纬度
        address.addDoubleProperty("longitude");             //经度

        try {
            //模版代码生成目录
            new DaoGenerator().generateAll(schema,"F:\\Code\\takeout\\app\\src\\main\\java");
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

}
