package com.itheima.takeout.model.dao;

import android.database.sqlite.SQLiteDatabase;

import com.itheima.common.base.Global;
import com.itheima.takeout.db.greendao.DaoMaster;
import com.itheima.takeout.db.greendao.DaoSession;

/**
 * Created by Administrator on 2017/4/27.
 *
 * @author hui
 */

public class GreenDaoHelper {

    private static GreenDaoHelper instance;
    private  DaoSession mDaoSession;

    public static GreenDaoHelper getInstance() {
        return SingleHolder.instance;
    }

    public  DaoSession getDaoSession() {
        return mDaoSession;
    }

    private GreenDaoHelper() {
        initDB();

    }
    // 初始化数据库
    private void initDB() {
        // DevOpenHelper -> SqliteDatabase -> DaoMaster  -> DaoSession -> 具体的某一个dao
        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(Global.mContext, "takeout.db", null);

        SQLiteDatabase database = helper.getReadableDatabase();

        DaoMaster master = new DaoMaster(database);

        mDaoSession = master.newSession();

    }

    private static class SingleHolder {
        public static GreenDaoHelper instance = new GreenDaoHelper();
    }


}
