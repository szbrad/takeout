package com.itheima.takeout.model.protocol;

import com.google.gson.JsonObject;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by Administrator on 2017/4/21.
 *
 * @author hui
 */

public interface IHttpService {
    String HOST_IP = "192.168.56.1";
    String HOST_URL = "http://" + HOST_IP + ":8080/TakeoutService/";
    String HTTP_GET_HOME = "home";
    String HTTP_GET_SHOP_LIST = "shopList";
    String HTTP_GET_SHOP_CATEGORY = "shopCategory";
    String HTTP_GET_SHOP_ORDERBY = "orderBy";
    String HTTP_GET_SHOP_DETAIL = "shopDetail";
    int GET_HOME = 0;
    int GET_SHOP_LIST = 1;
    int GET_SHOP_CATEGORY = 2;
    int GET_ORDERBY = 3;
    int GET_SHOP_DETAIL = 4;

    /**
     * 首页头部促销展示
     */
    @GET(HTTP_GET_HOME)
    Call<JsonObject> getHomeBean();

    /**
     * 获取首页商家所有类别
     */
    @GET(HTTP_GET_SHOP_CATEGORY)
    Call<JsonObject> getShopCategory();

    /**
     * 获取商家排序条件
     */
    @GET(HTTP_GET_SHOP_ORDERBY)
    Call<JsonObject> getOrderBy();


    /**
     * 获取商家所有商品及对应类别
     * @param shopId 商家ID
     * @return 商家的所有商品及对应的类别
     */
    @GET(HTTP_GET_SHOP_DETAIL)
    Call<JsonObject> getShopDetail(@Query("shopId")int shopId);

    /**
     * 首页周边商家列表
     *
     * @param map 商家的分类
     */
    @POST(HTTP_GET_SHOP_LIST)
    @FormUrlEncoded
    Call<JsonObject> getShopList(@FieldMap Map<String, Object> map);

}
