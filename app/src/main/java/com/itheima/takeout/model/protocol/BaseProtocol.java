package com.itheima.takeout.model.protocol;

import android.os.Message;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Administrator on 2017/4/21.
 *
 * @author hui
 */

public class BaseProtocol {


    private IHttpService mHttpService;

    public IHttpService getHttpService(){
        return mHttpService;
    }

    public BaseProtocol() {
        mHttpService = RetrofitManager.getInstance().getHttpService();
    }

    /**
     * 发送一个Http请求
     * @param call Retrofit 的一个Call对象
     * @param reqType 请求数据的类型
     * @param clazz  解析数据得到的实体对象
     * @param callback 回调接口
     * @param what 用作不同请求标识的一个参数 ,大部分情况下不会用
     * @param <T>
     */
    public <T> void execute(Call<JsonObject> call,
                            final int reqType,
                            final Class<T> clazz,
                            final HttpCallback callback , final int what){
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                //手动解析JSon
                String json = response.body().toString();
                try {
                    JSONObject object = new JSONObject(json);
                    //每个请求都会返回状态码 0 表示请求成功, 非0 表示失败,error是失败原因
                    if (object.getInt("status") == 0) {
                        Message message = Message.obtain();
                        //获取数据成功
                        T t = new Gson().fromJson(json,clazz);
                        message.obj = t;
                        message.what = what;
                        //请求数据成功
                        if (callback != null) {
                            callback.onResponse(reqType, message);
                        }
                    } else {
                        //获取数据失败
                        String error = object.getString("error");
                        //请求数据失败
                        if (callback != null) {
                            callback.onFailure(reqType, error);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    //请求数据失败
                    if (callback != null) {
                        callback.onFailure(reqType, e.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (callback != null) {
                    callback.onFailure(IHttpService.GET_HOME, t.getMessage());
                }
            }
        });
    }

    /**
     * 方法的重载
     * @param call Retrofit 的一个Call对象
     * @param reqType 请求数据的类型
     * @param clazz  解析数据得到的实体对象
     * @param callback 回调接口
     * @param <T>
     */
    public <T> void execute(Call<JsonObject> call,
                            final int reqType,
                            final Class<T> clazz,
                            final HttpCallback callback ){
        execute(call,reqType,clazz,callback,0);
    }

    /**
     * 请求数据的回调借口
     */
    public interface HttpCallback {
        /**
         * 请求消息成功
         *
         * @param reqType 请求数据的类型
         * @param msg     请求数据的消息
         */
        void onResponse(int reqType, Message msg);

        /**
         * 请求数据失败
         *
         * @param reqType 请求数据的类型
         * @param error   请求数据失败的原因
         */
        void onFailure(int reqType, String error);
    }
}
