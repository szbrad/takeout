package com.itheima.takeout.model.protocol;

import android.text.TextUtils;

import com.itheima.takeout.model.bean.HomeBean;
import com.itheima.takeout.model.bean.OrderBy;
import com.itheima.takeout.model.bean.ShopCategory;
import com.itheima.takeout.model.bean.ShopDetail;
import com.itheima.takeout.model.bean.ShopList;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 2017/4/21.
 *
 * @author hui
 */

public class CommonProtocol  extends BaseProtocol{


    /**
     * 获取一个HomeBean 请求
     * @param callback 请求的回调接口
     */
    public void getHomeBean(BaseProtocol.HttpCallback callback) {
        super.execute(super.getHttpService().getHomeBean(),IHttpService.GET_HOME, HomeBean.class,callback);
    }


    //经度
    private int latitude = 113;
    //纬度
    private int longitude = 13;

    /**
     * 获取一个ShopList 请求
     * @param callback 请求的回调接口
     */
    public void getShopListBean(BaseProtocol.HttpCallback callback,int pageNo,int pageCount) {
        if (TextUtils.isEmpty(String.valueOf(latitude))||TextUtils.isEmpty(String.valueOf(longitude))){
            throw new IllegalStateException("latitude与longitude 不能为空");
        }
        Map<String,Object> map  = new HashMap<>();
        map.put("latitude",latitude);
        map.put("longitude",longitude);
        map.put("pageNo",pageNo);
        map.put("pageCount",pageCount);
        //判断是否是加载首页
        int what = (pageNo==1)?1:0;
        super.execute(super.getHttpService().getShopList(map),IHttpService.GET_SHOP_LIST, ShopList.class,callback,what);
    }
    /**
     * 获取一个ShopList 请求
     * @param callback 请求的回调接口
     */
    public void getShopListBean(BaseProtocol.HttpCallback callback,int pageNo,int pageCount,int categoryId,int orderBy ) {
        if (TextUtils.isEmpty(String.valueOf(latitude))||TextUtils.isEmpty(String.valueOf(longitude))){
            throw new IllegalStateException("latitude与longitude 不能为空");
        }
        Map<String,Object> map  = new HashMap<>();
        map.put("latitude",latitude);
        map.put("longitude",longitude);
        map.put("pageNo",pageNo);
        map.put("pageCount",pageCount);
        map.put("categoryId",categoryId);
        map.put("orderBy",orderBy);
        //判断是否是加载首页
        int what = (pageNo==1)?1:0;
        super.execute(super.getHttpService().getShopList(map),IHttpService.GET_SHOP_LIST, ShopList.class,callback,what);
    }

    /**
     * 获取商家分类
     */
    public void getShopCateGory(HttpCallback callback){
        super.execute(super.getHttpService().getShopCategory(),IHttpService.GET_SHOP_CATEGORY, ShopCategory.class,callback);
    }

    /**
     * 获取商家排序条件
     */
    public void  getOrderBy(HttpCallback callback){
        super.execute(super.getHttpService().getOrderBy(),IHttpService.GET_ORDERBY, OrderBy.class,callback);
    }

    /**
     * 获取商家排序条件
     */
    public void  getShopDetail(HttpCallback callback,int shopId){
        super.execute(super.getHttpService().getShopDetail(shopId),IHttpService.GET_SHOP_DETAIL, ShopDetail.class,callback);
    }
}
