package com.itheima.takeout.model.bean.local;

/**
 * Created by Administrator on 2017/4/26.
 *
 * @author hui
 */

public class CartInfo {

    /** 购买总金额 */
    private float amount;

    /** 购买数量 */
    private int buyCount;

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public int getBuyCount() {
        return buyCount;
    }

    public void setBuyCount(int buyCount) {
        this.buyCount = buyCount;
    }
}
