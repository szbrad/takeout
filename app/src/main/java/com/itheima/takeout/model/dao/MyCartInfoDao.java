package com.itheima.takeout.model.dao;

import com.itheima.takeout.db.greendao.CartInfo;
import com.itheima.takeout.db.greendao.CartInfoDao;

import java.util.List;

import de.greenrobot.dao.query.WhereCondition;

/**
 * Created by Administrator on 2017/4/27.
 *
 * @author hui
 */

public class MyCartInfoDao {

    private CartInfoDao mCartInfoDao = GreenDaoHelper.getInstance().getDaoSession().getCartInfoDao();

    /**
     * 新增商品到购物车(新增一条记录)
     *
     * @param cartInfo
     */
    public void insertCartGoods(CartInfo cartInfo) {
        mCartInfoDao.insert(cartInfo);
    }


    /**
     * 从购物车删除一条记录
     *
     * @param goodsId 商品的id
     */
    public void deleteCartGoods(int goodsId) {
        WhereCondition whereCondition = CartInfoDao.Properties.GoodsId.eq(goodsId);
        List<CartInfo> list = mCartInfoDao.queryBuilder().where(whereCondition).build().list();

        if (list != null && list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                CartInfo cartInfo = list.get(i);
                mCartInfoDao.delete(cartInfo);
            }
        }
    }


    /**
     * 购买数量自增1
     *
     * @param goodsId 商品ID
     */
    public void increment(int goodsId) {
        WhereCondition whereCondition = CartInfoDao.Properties.GoodsId.eq(goodsId);
        List<CartInfo> list = mCartInfoDao.queryBuilder().where(whereCondition).build().list();

        if (list != null && list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                CartInfo cartInfo = list.get(i);
                cartInfo.setCount(cartInfo.getCount() + 1);
                mCartInfoDao.update(cartInfo);
            }
        }
    }

    /**
     * 购买数量自减1
     *
     * @param goodsId 商品ID
     */
    public void decrement(int goodsId) {
        WhereCondition whereCondition = CartInfoDao.Properties.GoodsId.eq(goodsId);
        List<CartInfo> list = mCartInfoDao.queryBuilder().where(whereCondition).build().list();

        if (list != null && list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                CartInfo cartInfo = list.get(i);
                cartInfo.setCount(cartInfo.getCount() - 1);
                mCartInfoDao.update(cartInfo);
            }
        }
    }


    /**
     * 清空某一商家的所有的购物车中的商品
     *
     * @param shopId 商家id
     */
    public void clearCartGoods(int shopId) {
        WhereCondition whereCondition = CartInfoDao.Properties.ShopId.eq(shopId);
        List<CartInfo> list = mCartInfoDao.queryBuilder().where(whereCondition).build().list();
        if (list != null && list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {

                mCartInfoDao.delete(list.get(i));
            }
        }
    }

    /**
     * 清空表中的所有数据
     */
    public void clearAll() {
        mCartInfoDao.deleteAll();
    }

    /**
     * 查询所有数据
     * @return  表中的所有数据集合
     */
    public List<CartInfo> queryAll() {
        return mCartInfoDao.queryBuilder().build().list();
    }


}