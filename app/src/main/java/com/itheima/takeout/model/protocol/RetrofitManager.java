package com.itheima.takeout.model.protocol;

import com.itheima.common.util.LogUtil;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Administrator on 2017/4/21.
 *
 * @author hui
 */

public class RetrofitManager {
    private static Retrofit mRetrofit;
    private static IHttpService mHttpService;

    private void initRetrofit() {
        OkHttpClient.Builder client = new OkHttpClient.Builder();
        if (LogUtil.mDebug){
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BASIC);
            client.addInterceptor(interceptor).connectTimeout(10, TimeUnit.SECONDS);
        }
        mRetrofit = new Retrofit.Builder().baseUrl(IHttpService.HOST_URL).addConverterFactory(GsonConverterFactory.create()).client(client.build()).build();
        mHttpService = mRetrofit.create(IHttpService.class);
    }

    public IHttpService getHttpService(){
        return mHttpService;
    }

    private RetrofitManager() {
        initRetrofit();
    }
    //===========================单例方式一=================================
//    private RetrofitManager instance = new RetrofitManager();
//    public RetrofitManager getInstance() {
//        return instance;
//    }

    //===========================单例方式二=================================
//    private RetrofitManager instance = null;
//    public RetrofitManager getInstance() {
//        if (instance == null) {
//            synchronized (this) {
//                if (instance == null) {
//                    instance = new RetrofitManager();
//                }
//            }
//        }
//        return instance;
//    }

    //===========================单例方式三(推荐)=================================

    private static class SingerHolder{
        public static RetrofitManager instance = new RetrofitManager();
    }
    public static RetrofitManager getInstance(){
        return SingerHolder.instance;
    }

}
