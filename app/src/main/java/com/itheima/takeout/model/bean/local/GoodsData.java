package com.itheima.takeout.model.bean.local;

import com.itheima.takeout.model.bean.ShopDetail;

import java.util.List;

/**
 * Created by Administrator on 2017/4/26.
 *
 * @author hui
 */

public class GoodsData {

    private List<ShopDetail.CategoryBean> mCategoryBeen;
    private List<ShopDetail.CategoryBean.GoodsBean> mAllGoods;

    public List<ShopDetail.CategoryBean> getCategoryBeen() {
        return mCategoryBeen;
    }

    public void setCategoryBeen(List<ShopDetail.CategoryBean> categoryBeen) {
        mCategoryBeen = categoryBeen;
    }

    public List<ShopDetail.CategoryBean.GoodsBean> getAllGoods() {
        return mAllGoods;
    }

    public void setAllGoods(List<ShopDetail.CategoryBean.GoodsBean> allGoods) {
        mAllGoods = allGoods;
    }
}
